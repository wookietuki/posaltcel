<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="icon" href="images/favicon.png">
	<title>..:: Recargas AltCl ::..</title>

	<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="{{ asset('css/materialize.css') }}">
	<link rel="stylesheet" href="{{ asset('css/lightbox.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">

	@yield('head')

</head>
<body>

	<!-- preloader -->
	<div class="preloader">
		<div class="spinner"></div>
	</div>
	<!-- end preloader -->

	<!-- navbar -->
	<div class="navbar">
		<div class="container">
			<div class="row">
				<div class="col s9">
					<div class="content-left">
						<a href="{{ URL::to('/') }}">
							<h1>Recargas<span> AltCl</span></h1>
						</a>
					</div>
				</div>
				<div class="col s3">
					<div class="content-right">
						<a href="#slide-out" data-activates="slide-out" class="sidebar"><i class="fa fa-bars"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end navbar -->

	<!-- sidebar -->
	<div class="sidebar-panel">
	</div>
	<!-- end sidebar -->
	
	<!-- slide -->
	@yield('slider')
	<!-- end slide -->

	<!-- menu -->
	@yield('menu-secundario')
	<!-- end menu -->

	<!-- content -->
	@yield('content')
	<!-- end content -->
	

	<!-- persipura -->
	<div class="persipura">
		<div class="container">
			<div class="jayapura">
				<div class="col s6">
					<div class="content">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end persipura -->

	<!-- vs -->
	<!-- end vs -->

	<!-- footer -->
	<footer>
		<div class="container">
			<ul>
				<!--<li><a href="fb://profile/100000160656231"><i class="fa fa-facebook"></i></a></li>
				<li><a href=""><i class="fa fa-twitter"></i></a></li>
				<li><a href=""><i class="fa fa-google"></i></a></li>
				<li><a href=""><i class="fa fa-instagram"></i></a></li>-->
			</ul>
			<p>Copyright © All Right Reserved</p>
		</div>
	</footer>
	<!-- end footer -->


	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('js/materialize.js') }}"></script>
	<script src="{{ asset('js/numscroller.js') }}"></script>
	<script src="{{ asset('js/lightbox.js') }}"></script>
	<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('js/main.js') }}"></script>

	@yield('scripts')

</body>
</html>