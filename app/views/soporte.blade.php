@extends('layouts.master')

@section('content')
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	    <div class="panel panel-default">
	        <div class="panel-heading" role="tab" id="heading1">
	            <h4 class="panel-title">
	                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
	                    iPhone
	                </a>
	            </h4>
	        </div>
	        <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
	            <div class="panel-body">
	                <p>
	                	<div>
									    	<b>DESCARGAR <b>Recargas AltCl</b></b> <br>

									    		Descargar <b>Recargas AltCl</b> para dispositivos <a href="itms-apps://apps.apple.com/mx/app/recargas-altcl/id1520253503">iPhone <i class="fas fa-external-link-alt"></i></a>. <br><br>
									    		<b><b>Recargas AltCl</b> está disponible para:</b> <br>
													<ul>
														<li>iPhone</li>
														<li>iOS 12.4 o versiones posteriores</li>
														<li>Idioma Español únicamente</li>
													</ul>
													<br>
									    		<b>Instalar <b>Recargas AltCl</b></b> <br>
									    		<ol>
									    			<li>En tu dispositivo iPhone o iPad, accede a <b>Recargas AltCl</b> en App Store.</li>
									    			<li>Toca Obtener.</li>
									    			<li>Toca Instalar.</li>
									    			<li>Introduce la contraseña de tu ID de Apple y toca OK.</li>
									    			<li>Para empezar a navegar, ve a la pantalla de inicio. Toca la aplicación <b>Recargas AltCl</b> <img src="{{ asset('images/icono.png') }}" alt="<b>Recargas AltCl</b>" width="18px"></li>
									    		</ol>
													 <br>
													Prueba a desinstalar <b>Recargas AltCl</b> e instalarlo de nuevo para solucionar problemas con tu motor de búsqueda, Flash, ventanas emergentes o actualizaciones del navegador. <br>
													<br>
													<b>Añadir <b>Recargas AltCl</b> al Dock de un iPhone</b> <br>
													Después de instalar <b>Recargas AltCl</b> en un iPhone o iPad, añádelo al Dock para buscarlo y abrirlo fácilmente: <br>	
													<ol>
														<li>Mantén pulsada una aplicación del Dock para liberar espacio.</li>
														<li>Arrástrala y suéltala en la pantalla principal.</li>
														<li>Mantén pulsada la aplicación <b>Recargas AltCl</b> <img src="{{ asset('images/icono.png') }}" alt="<b>Recargas AltCl</b>" width="18px"></li>
														<li>Arrástrala y suéltala en tu Dock.</li>
														<li>Pulsa el botón de inicio.</li>
													</ol>				
													
									    </div>
	                </p>
	            </div>
	        </div>
	    </div>
	    <div class="panel panel-default">
	        <div class="panel-heading" role="tab" id="heading2">
	            <h4 class="panel-title">
	                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
	                    Android
	                </a>
	            </h4>
	        </div>
	        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
	            <div class="panel-body">
	                <p>
	                	<div>
								    	Descargar <b>Recargas AltCl</b>
											Descarga <b>Recargas AltCl</b> para <a href="https://play.google.com/store/apps/details?id=com.altcel.posaltcel" target="_blank">teléfonos y tablets Android <i class="fas fa-external-link-alt"></i></a>. <br>

											<b>Recargas AltCl</b> está disponible para teléfonos y tablets con Android 7 (Nougat) o versiones posteriores. <br><br>

											<b>Instalar <b>Recargas AltCl</b></b> <br>
											<ol>
												<li>En tu teléfono o tablet Android, accede a <b>Recargas AltCl</b> en <a href="#" target="_blank">Google Play.</a></li>
												<li>Toca Instalar.</li>
												<li>Toca Aceptar.</li>
												<li>Para empezar a navegar, ve a la pantalla de inicio o al menú de aplicaciones. Toca la aplicación <b>Recargas AltCl</b> <img src="{{ asset('images/icono.png') }}" alt="Recargas AltCl" width="18px"></li>
											</ol>

											<br>
											Prueba a desinstalar <b>Recargas AltCl</b> e instalarlo de nuevo para solucionar problemas con tu buscador, Flash, ventanas emergentes o actualizaciones del navegador.
								    </div>
	                </p>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('scripts')
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
@endsection

@section('head')
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	
	<!------ Include the above in your HEAD tag ---------->
@endsection