@if (Session::has('errors'))
    <div class="callout callout-danger">
    <h4>Errores:</h4>
    @foreach ($errors->all() as $message)
        <i class="fas fa-times"></i> {{ $message }} <br>
    @endforeach
    </div>
@endif


@if (Session::has('success'))
	<div class="alert alert-success alert-dismissable">
    <i class="fas fa-check"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <b><i class="fas fa-thumbs-up"></i></b> {{ Session::get('success') }}
	</div>
@endif

@if (Session::has('error'))
	<div class="alert alert-danger alert-dismissable">
    <i class="fas fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <b><i class="fas fa-times"></i></b> {{ Session::get('error') }}
	</div>
@endif

@if (Session::has('info'))
    <div class="alert alert-info alert-dismissable">
      <i class="fas fa-check"></i>
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <b><i class="fas fa-info-sign"></i></b> {{ Session::get('info') }}
    </div>
@endif
