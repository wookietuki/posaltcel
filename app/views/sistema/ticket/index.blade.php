@extends('dashboard.layouts.dashboard.master')

{{ HTML::style('css/form.css') }}

{{ HTML::script('js/form.css') }}

@section('content')
@include('notifications')

	<div class="panel panel-info">
	  <div class="panel-heading">Comprobantes de Pago Últimos 10 Tickets</div>
	  <div class="panel-body">
	  	<div class="row">
				<div class="col-md-12">
					<a href="{{ URL::route('ticket.create') }}"><i class="fas fa-plus"></i> Agregar Comprobante de Pago</a>
				</div>
			</div>
			<br>
			<div class="table-responsive">
				<table class="table table-striped" id="tickets">
				  <thead>
				  	<tr>
				  		<th>Fecha</th>
				  		<th>Folio</th>
				  		<th>Compañia</th>
				  		<th>Cantidad</th>
				  		<th>Ticket</th>
              <th>Verificado</th>
				  	</tr>
				  </thead>
				  <tbody>
				  	@foreach ($tickets as $ticket)
					  	<tr class="text-center">
					  		<td nowrap>{{ $ticket->fecha }}</td>
					  		<td nowrap>{{ $ticket->folio }}</td>
					  		<td nowrap>{{ $ticket->compania }}</td>
					  		<td class="text-right" nowrap>$ {{ number_format($ticket->cantidad,2,'.',',') }}</td>
					  		<td nowrap>
					  			<a href="#" id="myImg{{ $ticket->id }}"><i class="fas fa-receipt fa-1x"></i></a>
					  		</td nowrap>
                <td>
                  @if ($ticket->verificado == 0)
                    <i class="fas fa-times" style="color: red;"></i>
                  @else
                    <i class="fas fa-check" style="color: green;"></i>
                  @endif
                </td>
					  	</tr>
					  	<div class="modal fade bs-example-modal-lg{{ $ticket->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
							  <div class="modal-dialog modal-lg">
							    <div class="modal-content">

							      <div class="modal-body">

							      <h1>¡Atención!</h1>
							      <h3>Se va a eliminar la Empresa: <b>{{ $ticket->nombre }}</b>.</h3>
							      <h3>¿Está seguro?</h3>
							      {{ Form::open(['route'=>['ticket.destroy',$ticket->id],'id'=>'myForm'.$ticket->id,'method'=>'DELETE']) }}
							        <a href="#" onclick="document.getElementById('myForm{{ $ticket->id }}').submit();" class="btn btn-danger btn-lg"><b><i class="fas fa-trash fa-2x"></i> Borrar</b></a>
							        <button type="button" class="btn btn-default btn-lg pull-right" data-dismiss="modal"><b><i class="fa fa-thumbs-up fa-2x"></i> Cancelar</b></button>
							      {{ Form::close() }}
							      </div>
							    </div>
							  </div>
							</div>

							<!-- The Modal -->
              <div id="myModal{{ $ticket->id }}" class="modal">

                <!-- The Close Button -->
                <span class="close" id="close{{ $ticket->id }}"> <i class="fas fa-times fa-2x"></i> </span>

                <!-- Modal Content (The Image) -->
                <img class="modal-content" id="img01{{ $ticket->id }}" width="70%">

                <!-- Modal Caption (Image Text) -->
                <div id="caption"></div>
              </div>

							<script type="text/javascript">
                // Get the modal
                var modal = document.getElementById('myModal{{ $ticket->id }}');

                // Get the image and insert it inside the modal - use its "alt" text as a caption
                var img = document.getElementById('myImg{{ $ticket->id }}');
                var modalImg = document.getElementById("img01{{ $ticket->id }}");
                img.onclick = function(){
                  modal.style.display = "block";
                  modalImg.src = '{{ asset($ticket->foto_ticket) }}';
                }

                // Get the <span> element that closes the modal
                var span = document.getElementById("close{{ $ticket->id }}");

                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                modal.style.display = "none";
                }
              </script>
							@endforeach
					</tbody>
				</table>
				</div>
	  </div>
	 </div>
@stop

@section('scripts')
	
	<!-- page script -->
  {{ HTML::script('//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js') }}
  {{ HTML::script('//cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.bootstrap.min.js') }}
  {{ HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}
  {{ HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js') }}
  {{ HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.colVis.min.js') }}

  <!-- page script -->
  <script type="text/javascript">
    $(function() {
      $('#tickets').dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "aaSorting": [[ 0, "desc" ]],
        /*"bInfo": true,
        "sDom": '<"top"Bif>rt<"bottom"pl><"clear">',
        "sButtons": [
          'copy', 'csv', 'excel', 'pdf', 'print'
        ],*/
        "bAutoWidth": true,
        "oLanguage": {
        "sLengthMenu": "_MENU_ tickets por página",
        "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_ tickets",
        "sEmptyTable": "No se encontraron datos en la tabla",
        "sInfoEmpty": "Mostrando del 0 al 0 de 0 tickets",
        "sInfoFiltered": "(filtrado de un total de _MAX_ tickets)",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Procesando...",
        "sSearch": "Buscar:",
        "sZeroRecords": "No se encontraron registros con la búsqueda",
        "oPaginate": {
          "sNext": "Siguiente",
          "sPrevious": "Anterior",
        }
        },
      });
    });
  </script>
@stop