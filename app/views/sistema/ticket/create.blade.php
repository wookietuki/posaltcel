@extends('dashboard.layouts.dashboard.master')

{{ HTML::style('css/form.css') }}

{{ HTML::script('js/form.css') }}



@section('content')
@include('notifications')

	<div class="panel panel-info">
	  <div class="panel-heading">Comprobantes de Pago</div>
	  <div class="panel-body">
	    <div class="row">
	    	<div class="col-md-12">
	    		<div id="loadingGif" style="display:none"><img src="https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif"></div>
	    		{{ Form::open(array('method' => 'post', 'files' => true, 'route'=>'ticket.store', 'id'=>'form', 'autocomplete'=>'off')) }}
			    	<div class="form-group float-label-control">
			    		<label>Fecha</label>
			    		{{-- Form::text('fecha', null, ['class'=>'form-control', 'required', 'placeholder'=>'Fecha','id'=>'fecha']) --}}
			    		<input type="date" name="fecha" value="{{ date('Y-m-d') }}" class="form-control" placeholder="Fecha" id="fecha">
			    		{{ $errors->first('fecha', '<p class="text-danger">:message</p>') }}
			    	</div>

			    	<div class="form-group float-label-control">
			    		<label>Folios</label>
			    		{{ Form::text('folio', null, ['class'=>'form-control', 'required', 'placeholder'=>'Folio','id'=>'folio']) }}
			    		{{ $errors->first('folio', '<p class="text-danger">:message</p>') }}
			    	</div>

			    	<div class="form-group float-label-control">
			    		<label>Cantidad Depositada</label>
			    		<input type="tel" name="cantidad" class="form-control" required placeholder="Cantidad Depositada" id="cantidad">
			    		{{ $errors->first('cantidad', '<p class="text-danger">:message</p>') }}
			    	</div>

						<div class="form-group">
							{{ Form::select('tipo',['Transferencia'=>'Transferencia','Depósito'=>'Depósito','Oxxo'=>'Oxxo','Efectivo'=>'Efectivo'],null,['class'=>'form-control','required']) }}
						</div>
						<div class="form-group">
							{{ Form::select('compania',[null=>'Selecciona uno','Altcel'=>'Altcel','Conecta'=>'Conecta'],null,['class'=>'form-control','required']) }}
						</div>

				    <div class="form-group">
				    	<label>Foto del Comprobante de Depósito</label>
				    	<input id='uploadImage' type='file' name='foto' required class="form-control" />
				    	{{ $errors->first('foto', '<p class="text-danger">:message</p>') }}
				    </div>

				    {{-- Form::submit('Guardar', array('class'=>'btn btn-primary','id'=>'submit-button','onclick'=>'showDiv()')) --}}
				    <button class="btn btn-danger btn-block button-loader btn-lg" data-loading-text="<i class='fa fa-circle-notch fa-spin'></i> Guardando..." type="submit">
						    <i class="fas fa-save"></i>
						    Guardar
						</button>
					{{ Form::close() }}
	    	</div>
	    </div>
	  </div>
	</div>

@endsection

@section('scripts')
	<script type="text/javascript">
  	
  	$("#form").validate({
      rules: {
        cantidad: {
          number: true,
          minlength: 2
        },
        fecha: {
        	date: true
        }
      }
    });

    $('#fecha1').datetimepicker({
				showTimePicker:false,
				format: 'yyyy-mm-dd',
	    	todayHighlight: true,
	    	autoclose: true,
				language: 'es',
				startView: 2,
				minView:2
			});

    

  </script>

  <script src="{{ asset('js/button-inline-loader.js') }}"></script>
	<script>
	  $('.btn').on('click', function(){
	  $('.button-loader').button('loading');
	  setTimeout(function(){ $('.button-loader').button('reset'); }, 5000);
	  $('#form').submit();
	})
	</script>
@endsection