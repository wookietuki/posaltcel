@extends('dashboard.layouts.dashboard.master')


{{ HTML::style('css/form.css') }}

{{ HTML::script('js/form.css') }}

@section('content')
@include('notifications')
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-danger">
				<div class="panel-body">
					<h3>
            <?php $icon = end($breadcrumb)['icon']; ?>
            <i class="<?php echo (strpos($icon,'fa-')!==false)?'fa':'glyphicon '; ?> {{$icon}}"></i>
            {{end($breadcrumb)['title']}}
					</h3>
					<hr>
					<?php
						if (isset($telefono)) {
							$method = 'PUT';
							$route = ['recurrente.telefono.update',$telefono->id];
						} else {
							$method = 'POST';
							$route = 'recurrente.telefono.store';
						}
					?>
					{{ Form::open(['id'=>'form','method'=>$method,'route'=>$route,'role'=>'form','autocomplete'=>'off']) }}

						{{ Form::hidden('empresa_id', $empresa_id) }}

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Teléfono</label>
			              {{ Form::text('telefono', Input::old('telefono', isset($telefono)?$telefono->telefono:null), ['class'=>'form-control', 'required', 'placeholder'=>'Teléfono']) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
										Monto de Recarga <br>
			              <label for="">Monto de Recarga</label>
			              {{ Form::select('cant_recarga_id', [null=>'Seleccione un Monto de Recarga']+$montos, Input::old('cant_recarga_id', isset($telefono)?$telefono->cant_recarga_id:null), array('class'=>'form-control required')) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Fecha Alta</label>
			              {{ Form::text('alta', Input::old('alta', isset($telefono)?$telefono->alta:null), ['class'=>'form-control', 'required', 'placeholder'=>'Fecha Alta','id'=>'alta']) }}
			            </div>					
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<div class="form-group float-label-control">
			              Recurrencia 
			              <label for="">Recurrencia</label><!-- 7,30,40 -->
			              {{ Form::select('recurrencia', [null=>'Selecciona días de Recurrencia','7'=>'7','30'=>'30','40'=>'40'], Input::old('recurrencia', isset($telefono)?$telefono->recurrencia:null), array('class'=>'form-control required')) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
			              Activo <br>
                    	{{ Form::checkbox('activo', Input::old('activo', isset($telefono)?$telefono->activo:null), isset($telefono)?$telefono->activo==1?'ckecked':null:'checked', ['class'=>'form-control flat-red']) }}
			            </div>					
								</div>

								@if (isset($telefono))
									<div class="col-md-4">
										<div class="form-group float-label-control">
				              <label for="">Fecha Última Recarga</label>
				              {{ Form::text('nada', Input::old('nada', isset($telefono)?$telefono->ultima:null), ['class'=>'form-control','readonly', 'placeholder'=>'Fecha Última Recarga','id'=>'ultima']) }}
				            </div>					
									</div>
								@endif

							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									{{ Form::submit('Guardar', array('class'=>'btn btn-primary')) }}
								</div>
							</div>
						</div>
	
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script type="text/javascript">
  	
  	$("#form").validate({
      rules: {
        telefono: {
          digits: true,
          minlength: 10,
          maxlength:10
        },
        alta: {
        	date: true
        }
      }
    });

    $('#alta').datetimepicker({
				showTimePicker:false,
				format: 'yyyy-mm-dd',
	    	todayHighlight: true,
	    	autoclose: true,
				language: 'es',
				startView: 2,
				minView:2
			});

  </script>
@endsection