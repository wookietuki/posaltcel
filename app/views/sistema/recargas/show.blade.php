<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Comprobante</title>
  </head>
  <body>
    <div class="content">
      <div class="row justify-content-center">
        <div class="col-8 text-center">
          <img src="{{ asset('images/logo1.png') }}" width="330px" alt="">
          <br>
          <br>
          <div class="alert alert-{{ $recarga->role }}" role="alert">
            <b>{{ $recarga->descriptioncode }}</b>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-8">
          <table class="table">
            <tr>
              <td>Fecha y Hora:</td>
              <td>{{ $recarga->fecha }}</td>
            </tr>
            <tr>
              <td># de transacción:</td>
              <td>{{ $recarga->transnumber }}</td>
            </tr>
            <tr>
              <td># de Autorización:</td>
              <td>{{ $recarga->autono }}</td>
            </tr>
            <tr>
              <td>Monto de la Recarga:</td>
              <td>$ {{ number_format($recarga->qty,2,'.',',') }}</td>
            </tr>
            <tr>
              <td>Teléfono:</td>
              <td>{{ $recarga->phonenumber }}</td>
            </tr>
            <tr>
              <td width="45%">Observaciones:</td>
              <td class="text-justify">{{ $recarga->instr1 }} <br> <br> {{ $recarga->instr2 }}</td>
            </tr>
            <tr class="no-print d-print-none">
              <td colspan="2">
                <a class="btn btn-info" href="{{ URL::to('altcel') }}">Regresar</a>
              </td>
            </tr>
          </table>
          <br><br><br><br>
          <hr>
        </div>
      </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="//code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>