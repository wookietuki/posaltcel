@extends('dashboard.layouts.dashboard.master')

@section('content')
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-danger">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<h1>Transacciones de Recargas de los últimos 30 días</h1>
						</div>
					</div>
					<br>
					<table class="table table-striped" id="recargas">
					  <thead>
					  	<tr>
					  		<th>Id</th>
					  		<th>Día Hora</th>
					  		<th>Distribuidor</th>
					  		<th>Teléfono</th>
					  		<th>Monto</th>
					  		<th>Transacción Altcel</th>
					  		<th>Autorización TN</th>
					  		<th>Código de Respuesta</th>
					  		<th>Descripción</th>
					  	</tr>
					  </thead>
					  <tbody>
					  	@foreach ($recargas as $recarga)
						  	<tr>
						  		<td>{{ $recarga->id }}</td>
						  		<td>{{ $recarga->fecha }}</td>
						  		<td>{{ isset($recarga->distribuidor->distribuidor)?$recarga->distribuidor->distribuidor:'Otro - '. $recarga->distribuidor_id }}</td>
						  		<td class="text-right">{{ $recarga->phonenumber }}</td>
						  		<td class="text-right">$ {{ number_format($recarga->qty,2,'.',',') }}</td>
						  		<td class="text-center">{{ $recarga->transnumber }}</td>
						  		<td class="text-center">{{ $recarga->autono }}</td>
						  		<td>{{ $recarga->responsecode }}</td>
						  		<td>{{ $recarga->descriptioncode }}</td>
						  	</tr>
					  	@endforeach
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	
	<!-- page script -->
  {{ HTML::script('//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js') }}
  {{ HTML::script('//cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.bootstrap.min.js') }}
  {{ HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}
  {{ HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js') }}
  {{ HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.colVis.min.js') }}

  <!-- page script -->
  <script type="text/javascript">
    $(function() {
      $('#recargas').dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "aaSorting": [[ 0, "desc" ]],
        "bInfo": true,
        "sDom": '<"top"Bif>rt<"bottom"pl><"clear">',
        "sButtons": [
          'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "bAutoWidth": true,
        "oLanguage": {
        "sLengthMenu": "_MENU_ recargas por página",
        "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_ recargas",
        "sEmptyTable": "No se encontraron datos en la tabla",
        "sInfoEmpty": "Mostrando del 0 al 0 de 0 recargas",
        "sInfoFiltered": "(filtrado de un total de _MAX_ recargas)",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Procesando...",
        "sSearch": "Buscar:",
        "sZeroRecords": "No se encontraron registros con la búsqueda",
        "oPaginate": {
          "sNext": "Siguiente",
          "sPrevious": "Anterior",
        }
        },
      });
    });
  </script>
@stop