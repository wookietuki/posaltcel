@extends('dashboard.layouts.dashboard.master')


{{ HTML::style('css/form.css') }}

{{ HTML::script('js/form.css') }}

@section('content')
@include('notifications')
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-danger">
				<div class="panel-body">
					<h3>
            <?php $icon = end($breadcrumb)['icon']; ?>
            <i class="<?php echo (strpos($icon,'fa-')!==false)?'fa':'glyphicon '; ?> {{$icon}}"></i>
            {{end($breadcrumb)['title']}}
					</h3>
					<hr>
					<?php
						if (isset($conecta_cliente)) {
							$method = 'PUT';
							$route = ['conecta_cliente.update',$conecta_cliente->id];
						} else {
							$method = 'POST';
							$route = 'conecta_cliente.store';
						}
					?>
					{{ Form::open(['id'=>'form','method'=>$method,'route'=>$route,'role'=>'form']) }}

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group float-label-control">
			              <label for="">Nombre</label>
			              {{ Form::text('nombre_cliente', Input::old('nombre_cliente', isset($conecta_cliente)?$conecta_cliente->nombre_cliente:null), ['class'=>'form-control', 'required', 'placeholder'=>'Nombre']) }}
			            </div>					
								</div>

								<div class="col-md-6">
									<div class="form-group float-label-control">
			              <label for="">Apellido</label>
			              {{ Form::text('apellido_cliente', Input::old('apellido_cliente', isset($conecta_cliente)?$conecta_cliente->apellido_cliente:null), ['class'=>'form-control', 'required', 'placeholder'=>'Apellido']) }}
			            </div>					
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Teléfono Altcel</label>
			              {{ Form::text('tel_altcel', Input::old('tel_altcel', isset($conecta_cliente)?$conecta_cliente->tel_altcel:null), ['class'=>'form-control', 'required', 'placeholder'=>'Teléfono Altcel']) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Teléfono Conecta</label>
			              {{ Form::text('tel_altan', Input::old('tel_altan', isset($conecta_cliente)?$conecta_cliente->tel_altan:null), ['class'=>'form-control', 'required', 'placeholder'=>'Teléfono Conecta',isset($conecta_cliente)?'Readonly':'']) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Correo Electrónico</label>
			              {{ Form::email('email', Input::old('email', isset($conecta_cliente)?$conecta_cliente->email:null), ['class'=>'form-control', 'required', 'placeholder'=>'Correo Electrónico',isset($conecta_cliente)?'Readonly':'']) }}
			            </div>					
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									{{ Form::submit('Guardar', array('class'=>'btn btn-primary')) }}
								</div>
							</div>
						</div>
	
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script type="text/javascript">
  	
  	$("#form").validate();

  </script>
@endsection