@extends('dashboard.layouts.dashboard.master')

@section('content')

@include('notifications')


<div class="col-md-12">
    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title">Paquete Excedente</h3>
        </div>
        <div class="saldo_conecta">
        <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        $ {{ number_format($userDist->saldo_conecta,2,'.',',') }}
                    </h3>
                    <p>
                        SALDO
                    </p>
                </div>
                <div class="icon">
                    <i class="fas fa-cash-register"></i>
                </div>
                
            </div>
        </div><!-- ./col -->
        <div class="box-body" style="padding-bottom: 1px !important;">
            <div class="alert alert-info alert-dismissable">
                <i class="fa fa-info" style="margin-right: 0.5rem;"></i><b>DATOS DEL CLIENTE</b><i class="fa fa-info" style="margin-left: 0.5rem;"></i><br>
                    <span>Cliente: <b>{{$dataMSISDN['name_user'].' '.$dataMSISDN['lastname_user']}}</b></span><br>
                    <span>Email: <b>{{$dataMSISDN['email_user']}}</b></span><br>
                    <span>Plan Activo: <b>{{$dataMSISDN['rate_name']}}</b></span>
            </div>
            <form role="form">
               
                <div class="form-group">
                    <label>Número</label>
                    <input type="text" class="form-control" placeholder="Enter ..." disabled="" id="msisdn" value="{{$dataMSISDN['MSISDN']}}">
                </div>

                <input type="hidden" id="user_id" value="{{$user->id}}">

                <div class="form-group">
                    <label>Paquetes Disponibles</label>
                    <select class="form-control" id="rates-surplus">
                        <option value="0">Elige uno...</option>
                        @foreach($packsSurplus as $packSurplus)
                            <option value="{{$packSurplus['offerID']}}"
                            data-rate-price="{{$packSurplus['price_sale']}}"
                            data-rate-id="{{$dataMSISDN['rate_id']}}" 
                            data-offer-id="{{$packSurplus['id']}}"
                            data-rate-name="{{$packSurplus['name']}}">
                            {{$packSurplus['name'].' - $'.number_format($packSurplus['price_sale'],2)}}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="callout callout-warning d-none" id="warning-rate">
                    <strong>Verifique los datos de la recarga</strong>
                    <ul>
                        <li id="pack-warning">Paquete: Recarga MIFI 1GB / 20GB Roaming</li>
                        <li>SIM: {{$dataMSISDN['MSISDN']}}</li>
                        <li id="price-warning"><strong>Importe: 30.00</strong></li>
                    </ul>
                </div>

            </form>
        </div>
        <div class="box-footer">
            <button type="button" class="btn btn-success" id="goPurchase" disabled>Aceptar</button>
        </div>
    </div>
</div>

<script>

    $('#rates-surplus').change(function(){
        let offerID =  $(this).val();
        let rate_id = $('#rates-surplus option:selected').attr('data-rate-id');
        let price = $('#rates-surplus option:selected').attr('data-rate-price');
        let offer_id = $('#rates-surplus option:selected').attr('data-offer-id');
        let rate_name = $('#rates-surplus option:selected').attr('data-rate-name');

        if(offerID == 0){
            $('#warning-rate').addClass('d-none');
            $('#goPurchase').attr('disabled',true);
        }else{
            $('#pack-warning').html('Paquete: '+rate_name);
            $('#price-warning').html('<strong>Importe: '+parseFloat(price).toFixed(2)+'</strong>');
            $('#warning-rate').removeClass('d-none');
            $('#goPurchase').attr('disabled',false);
        }
    });

    $('#goPurchase').click(function(){
        let msisdn = $('#msisdn').val();
        let offerID = $('#rates-surplus').val();
        let rate_id = $('#rates-surplus option:selected').attr('data-rate-id');
        let price = $('#rates-surplus option:selected').attr('data-rate-price');
        let offer_id = $('#rates-surplus option:selected').attr('data-offer-id');
        let comment = '', reason = 'cobro', status = 'completado';
        // let user_id = $('#user_id').val();
        let user_id = 253;
        let data;

        if(offerID == 0){
            console.log('NO ELIGIÓ NOTHING');
        }else{
            data = {
                msisdn:msisdn,
                offerID:offerID,
                rate_id:rate_id,
                offer_id:offer_id,
                user_id:user_id,
                price:price, 
                comment:comment,
                reason:reason,
                status:status
            }

            $.ajax({
                url: "{{URL::route('altcel2.purchase')}}",
                data: data,
                success: function(response){
                    console.log(response);
                }
            });

            console.log(data);
        }
    })
</script>

@endsection