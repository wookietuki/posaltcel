@extends('dashboard.layouts.dashboard.master')

@section('content')

@include('notifications')
<div class="col-lg-4 col-xs-12">
<!-- small box -->
    <div class="small-box bg-green">
        <div class="inner">
            <h3>
                $ {{ number_format($userDist->saldo_conecta,2,'.',',') }}
            </h3>
            <p>
                SALDO
            </p>
        </div>
        <div class="icon">
            <i class="fas fa-cash-register"></i>
        </div>
        <!-- <a href="{{ URL::route('recarga.index') }}" class="small-box-footer">
            Ver <i class="fa fa-arrow-circle-right"></i>
        </a> -->
    </div>
</div><!-- ./col -->

<input type="hidden" id="available" value="{{$userDist->saldo_conecta}}">
<input type="hidden" id="local_user_id" value="{{$user->id}}">

<div class="col-md-6 col-md-offset-3" style="clear: both;">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title text-center">Movimientos <img src="{{ asset('images/logo1.png') }}" width="80px" alt="Altcel"></h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <form>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group float-label-control">
                        <input type="tel" name="telefono" value="" class="form-control" required placeholder="Número de Teléfono" id="msisdn" maxlength="10">
                      </div>          
                    </div>

                    <div class="col-md-6">
                      <div class="form-group float-label-control">
                        <input type="tel" name="tel_conf" value="" class="form-control" required placeholder="Confirma tu Número" id="msisdn2" maxlength="10">

                      </div>          
                    </div>

                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12" style="display: flex; justify-content:center !important;">
                    <button type="button" class="btn btn-success btn-sm movements" disabled id="monthly" data-type="monthly" style="margin-right: 1rem; margin-top: 1rem;">Pago Mensual</button>
                    <button type="button" class="btn btn-info btn-sm movements" disabled id="surplus" data-type="surplus" style="margin-right: 1rem; margin-top: 1rem;">Excedente</button>
                    <!-- <button type="button" class="btn btn-warning btn-sm movements" disabled id="change" data-type="change" style="margin-right: 1rem; margin-top: 1rem;">Cambio de Plan</button> -->
                  </div>
                </div>
                <hr>
                <div class="row" style="margin-top: 1rem;" >
                    <div class="col-md-12">
                        <div class="col-md-6 d-none" id="ratesSelectContent">
                            <div class="form-group float-label-control">
                                <label for="">Paquetes Disponibles</label>
                                <select class="form-control" id="ratesSelect">
                                </select>
                            </div>          
                        </div>
                        <div class="col-md-6 d-none" id="montoExtraContent">
                            <div class="form-group float-label-control">
                                <input type="text" value="0.00" class="form-control" required placeholder="Monto Extra" id="montoExtra">
                                <label for="">Monto Extra</label>
                            </div>          
                        </div>
                    </div>
                </div>
                <div class="callout callout-warning d-none" id="warning-rate">
                    <strong>Verifique los datos de la recarga</strong>
                    <ul>
                        <li id="pack-warning"></li>
                        <li id="msisdn-warning"></li>
                        <li id="import-warning"><strong></strong></li>
                    </ul>
                </div>

                <div class="callout callout-info d-none" id="warning-info-monthly">
                    <center>
                        <h4>Información de la SIM</h4>
                        <span class="text-light-blue" id="subtitle-payment"></span>
                    </center>
                    <strong id="days-to-expirate"></strong><br>
                    <strong id="date_pay"></strong><br>
                    <strong id="date_pay_limit"></strong><br>
                    <strong id="date_expirate"></strong><br><br>

                    <center>
                        <span>Unidades del paquete contratado:</span>
                    </center>
                    <strong id="freeUnitsTotal"></strong><br>
                    <strong id="freeUnits"></strong><br>
                    <strong id="freeUnitsPercentage"></strong><br>
                    <div class="progress sm progress-striped active">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" id="progress_bar_one" style="width: 20%"></div>
                    </div>

                    <center>
                        <span>Unidades de recarga:</span>
                    </center>
                    <strong id="freeUnitsTotal2"></strong><br>
                    <strong id="freeUnits2"></strong><br>
                    <strong id="freeUnitsPercentage2"></strong><br>
                    <div class="progress sm progress-striped active">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" id="progress_bar_two" style="width: 20%"></div>
                    </div>
                    
                </div>
                <input type="hidden" id="lat">
                <input type="hidden" id="lng">
                <input type="hidden" id="product">
                <input type="hidden" id="payID">
                <input type="hidden" id="monto">

                <!-- RESPALDO DE DATOS PARA SELECT EN CASO DE ELEGIR MENSUALIDAD Y CAMBIO -->
                <input type="hidden" id="data-rate-id">
                <input type="hidden" id="data-rate-price">
                <input type="hidden" id="data-offerID">
                <input type="hidden" id="data-offer-id">
                <input type="hidden" id="data-rate-name">
                <!-- END RESPALDO DE DATOS PARA SELECT EN CASO DE ELEGIR MENSUALIDAD Y CAMBIO -->

                <div class="row">
                  <div class="col-md-12" style="display: flex; justify-content:center !important;">
                    <button type="button" class="btn btn-success btn-sm" id="execute" style="margin-right: 1rem; margin-top: 1rem;" disabled>Ejecutar</button>
                  </div>
                </div>
                </form>
            </div>
          </div>
        </div>
      </div>


<script>
    var dealer_id = 0;

    $('.movements').click(function(){
        let type = $(this).data('type');
        let msisdn = $('#msisdn').val();
        let urlSurplus = "{{URL::route('altcel2.surplus',['msisdn'=>'temp'])}}";
        let urlChange = "{{URL::route('altcel2.get.offers.rates.diff',['msisdn'=>'temp'])}}";
        let urlMonthly = "{{URL::route('altcel2.monthly',['msisdn'=>'temp'])}}";
        let options = '<option value="0">Elige uno...</option>';
        urlSurplus = urlSurplus.replace('temp',msisdn);
        urlChange = urlChange.replace('temp',msisdn);
        urlMonthly = urlMonthly.replace('temp',msisdn);

        if(type == 'surplus'){
            $.ajax({
                url: urlSurplus,
                beforeSend: function(){
                    Swal.fire({
                        title: 'Estamos extrayendo las recargas disponibles...',
                        html: 'Espera un poco, un poquito más...',
                        showConfirmButton: false,
                        didOpen: () => {
                            Swal.showLoading();
                        }
                    });
                },
                success: function(response){
                    let packSurplus = response.packsSurplus;
                    
                    packSurplus.forEach(function(element){
                        options+='<option value="'+element.offerID+'" data-type="surplus" data-rate-price="'+element.price_sale+'" data-rate-id="'+response.dataMSISDN.rate_id+'" data-offer-id="'+element.id+'" data-rate-name="'+element.name+'">'+element.name+' - $'+parseFloat(element.price_sale).toFixed(2)+'</option>'
                    });
                    $('#ratesSelect').html(options);
                    $('#ratesSelectContent').removeClass('d-none');
                    $('#montoExtraContent').addClass('d-none');
                    $('#warning-info-monthly').addClass('d-none');
                    Swal.close();
                    $('#execute').attr('disabled',false);
                }
            });
        }else if(type == 'change'){
            
            $.ajax({
                url: urlChange,
                beforeSend: function(){
                    Swal.fire({
                        title: 'Estamos extrayendo las tarifas disponibles...',
                        html: 'Espera un poco, un poquito más...',
                        showConfirmButton: false,
                        didOpen: () => {
                            Swal.showLoading();
                        }
                    });
                },
                success: function(response){
                    response = JSON.parse(response);
                    let offersAndRates = response.offersAndRates;
                    console.log(offersAndRates);
                    $('#lat').val(response.dataMSISDN.lat);
                    $('#lng').val(response.dataMSISDN.lng);
                    $('#product').val(response.dataMSISDN.producto);
                    
                    offersAndRates.forEach(function(element){
                        if(element.rate_price > 0 && !element.rate_name.includes('F7')){
                            options+="<option value='"+element.rate_id+"' data-type='change' data-rate-id='"+response.dataMSISDN.rate_id+"' data-rate-price='"+element.rate_price+"' data-offerID='"+element.offerID+"' data-offer-id='"+element.offer_id+"' data-rate-name='"+element.rate_name+"'>"+element.rate_name+" - $"+parseFloat(element.rate_price).toFixed(2)+"</option>"
                        }
                    })
                    $('#ratesSelect').html(options);
                    $('#ratesSelectContent').removeClass('d-none');
                    $('#montoExtraContent').addClass('d-none');
                    $('#warning-info-monthly').addClass('d-none');
                    Swal.close();
                    $('#execute').attr('disabled',false);
                }
            });
        }else if(type =='monthly'){
            $.ajax({
                url: urlMonthly,
                beforeSend: function(){
                    Swal.fire({
                        title: 'Estamos extrayendo la información necesaria...',
                        html: 'Espera un poco, un poquito más...',
                        showConfirmButton: false,
                        didOpen: () => {
                            Swal.showLoading();
                        }
                    });
                },
                success: function(response){
                    $('#payID').val(response.information.pay_id);
                    $('#product').val(response.information.product);
                    $('#monto').val(response.information.amount);
                    $('#pack-warning').html('Plan a pagar: '+response.information.rate_name);
                    $('#msisdn-warning').html('SIM: '+msisdn);
                    $('#import-warning').html('Importe: $'+parseFloat(response.information.amount).toFixed(2));

                    $('#ratesSelect').html("<option selected value='"+response.information.rate_id+"' data-type='monthly' data-rate-id='"+response.information.rate_id+"' data-rate-price='"+response.information.amount+"' data-offerID='"+response.information.offerID+"' data-offer-id='"+response.information.offer_id+"' data-rate-name='"+response.information.rate_name+"'>"+response.information.rate_name+" - $"+parseFloat(response.information.amount).toFixed(2)+"</option>");

                    if(response.information.http_code != 0 && response.information.http_code != 3){
                        $('#warning-info-monthly').addClass('d-none');
                        $('#data-rate-id').val(response.originalData[0].rate_id);
                        $('#data-rate-price').val(response.originalData[0].rate_price);
                        $('#data-offerID').val(response.originalData[0].offerID);
                        $('#data-offer-id').val(response.originalData[0].offer_id);
                        $('#data-rate-name').val(response.originalData[0].rate_name);
                        $('#lat').val(response.originalData[0].lat_hbb);
                        $('#lng').val(response.originalData[0].lng_hbb);

                        $('#ratesSelectContent').removeClass('d-none');
                        $('#montoExtraContent').removeClass('d-none');
                        $('#execute').attr('disabled',false);
                    }else if(response.information.http_code == 0 || response.information.http_code == 3){
                        $('#ratesSelectContent').addClass('d-none');
                        $('#montoExtraContent').addClass('d-none');

                        $('#subtitle-payment').html('Aún falta para la fecha de pago.');
                        $('#days-to-expirate').html('Faltan '+response.information.days+' días.');
                        $('#date_pay').html('Fecha de pago: '+response.information.datePayment);
                        $('#date_pay_limit').html('Fecha límite de pago: '+response.information.dateLimit);
                        $('#date_expirate').html('Renueva el '+response.information.renovationDate);

                        let progressOne = $('#progress_bar_one');
                        let progressTwo = $('#progress_bar_two');

                        $('#freeUnitsTotal').html('Totales: '+parseFloat(response.APIData.FreeUnits.totalAmt).toFixed(2)+'GB');
                        $('#freeUnits').html('Libres: '+parseFloat(response.APIData.FreeUnits.unusedAmt).toFixed(2)+'GB');
                        $('#freeUnitsPercentage').html('Porcentaje libre: '+parseInt(response.APIData.FreeUnits.freePercentage)+'%');
                        progressOne.css('width',response.APIData.FreeUnits.freePercentage+'%');

                        $('#freeUnitsTotal2').html('Totales: '+parseFloat(response.APIData.FreeUnits2.totalAmt).toFixed(2)+'GB');
                        $('#freeUnits2').html('Libres: '+parseFloat(response.APIData.FreeUnits2.unusedAmt).toFixed(2)+'GB');
                        $('#freeUnitsPercentage2').html('Porcentaje libre: '+parseInt(response.APIData.FreeUnits2.freePercentage)+'%');
                        progressTwo.css('width',response.APIData.FreeUnits2.freePercentage+'%');

                        $('#warning-info-monthly').removeClass('d-none');
                        $('#execute').attr('disabled',true);
                    }

                    Swal.close();
                }
            });
        }

    });

    $('#execute').click(function(){
        let type = $('#ratesSelect option:selected').attr('data-type');
        let saldo = $('#available').val();
        saldo = parseFloat(saldo);
        let importe = $('#ratesSelect option:selected').attr('data-rate-price');
        importe = parseFloat(importe);
        dealer_id = $('#local_user_id').val();

        if(saldo < importe){
            Swal.fire({
                icon: 'error',
                title: 'Woops!',
                text: 'No cuentas con saldo suficiente para realizar este movimiento. :(',
                showConfirmButton: false,
                timer: 3000
            });
            return false;
        }

        if(type == 'surplus'){
            executeSurplus();
        }else if(type == 'change'){
            executeChange();
        }else if(type == 'monthly'){
            executeMonthly();
        }
    });

    function executeSurplus(){
        let msisdn = $('#msisdn').val();
        let offerID = $('#ratesSelect').val();
        let rate_id = $('#ratesSelect option:selected').attr('data-rate-id');
        let price = $('#ratesSelect option:selected').attr('data-rate-price');
        let offer_id = $('#ratesSelect option:selected').attr('data-offer-id');
        let comment = '', reason = 'cobro', status = 'completado';
        // let user_id = $('#user_id').val();
        let user_id = 253;
        let data;

        data = {
            msisdn:msisdn,
            offerID:offerID,
            rate_id:rate_id,
            offer_id:offer_id,
            user_id:user_id,
            price:price, 
            comment:comment,
            reason:reason,
            status:status,
            dealer_id:dealer_id
        }

        $.ajax({
            url: "{{URL::route('altcel2.purchase')}}",
            data: data,
            beforeSend: function(){
                Swal.fire({
                    title: 'Realizando compra...',
                    html: 'Espera un poco, un poquito más...',
                    showConfirmButton: false,
                    didOpen: () => {
                        Swal.showLoading();
                    }
                });
            },
            success: function(response){
                // console.log(response.message);
                if(response.http_code == 1){
                    Swal.fire({
                        icon: 'success',
                        title: 'Hecho',
                        text: response.message,
                        showConfirmButton: false,
                        timer: 2000
                    });
                    setTimeout(function(){ location.reload();},3000);
                }else if(response.http_code == 2){
                    Swal.fire({
                        icon: 'error',
                        title: 'Woops!',
                        text: response.message
                    });
                    setTimeout(function(){ location.reload();},3000);
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Woops!',
                        text: response.message
                    });
                }
            }
        });

        console.log(data);
    }

    function executeChange(){
        let msisdn = $('#msisdn').val();
        let rate = $('#ratesSelect').val();
        let offerID = $('#ratesSelect option:selected').attr('data-offerid');
        let offer_id = $('#ratesSelect option:selected').attr('data-offer-id');
        let type = "internalExternalChange";
        let producto = $('#product').val();
        let amount = $('#ratesSelect option:selected').attr('data-rate-price');
        let scheduleDate = '';
        let user_id = 253;
        let lat = '', lng = '', address = '';
        let data;

        let comment = '', reason = 'cobro', status = 'completado';

        if(producto == 'HBB'){
            lat = $('#lat').val();
            lng = $('#lng').val();
            address = lat+','+lng;
        }else{
            address = null;
        }
        data = {
            msisdn:msisdn, 
            rate_id:rate, 
            offerID:offerID, 
            offer_id:offer_id, 
            type:type, 
            scheduleDate:scheduleDate, 
            address:address,
            producto:producto,
            user_id:user_id,
            amount:amount,
            comment:comment,
            reason:reason,
            status:status,
            payID:0,
            dealer_id:dealer_id
        }

        $.ajax({
            url: "{{URL::route('altcel2.change.product')}}",
            data: data,
            beforeSend: function(){
                Swal.fire({
                    title: 'Realizando cambio de plan...',
                    html: 'Espera un poco, un poquito más...',
                    showConfirmButton: false,
                    didOpen: () => {
                        Swal.showLoading();
                    }
                });
            },
            success: function(response){
                // console.log(response.message);
                if(response.http_code == 1 || response.http_code == 2){
                    Swal.fire({
                        icon: 'success',
                        title: 'Hecho',
                        text: response.message,
                        showConfirmButton: false,
                        timer: 2000
                    });
                    setTimeout(function(){ location.reload();},3000);
                }else if(response.http_code == 3){
                    Swal.fire({
                        icon: 'error',
                        title: 'Woops!',
                        text: response.message
                    });
                    setTimeout(function(){ location.reload();},3000);
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Woops!',
                        text: response.message
                    });
                }
            }
        });
        // console.log('Procede el cambio de producto');
    }

    function executeMonthly(){
        let msisdn = $('#msisdn').val();
        let originalRate = $('#data-rate-id').val();
        let selectedRate = $('#ratesSelect').val();
        let service = $('#product').val();
        let payID = $('#payID').val();
        let monto = $('#monto').val();
        let montoExtra = $('#montoExtra').val();
        let typePay = 'efectivo', folioPay  = 'N/A', estadoPay = 'completado', user_id = 253;
        let data;

        if(originalRate == selectedRate){

            data = {
                service: service,
                payID: payID,
                monto: monto,
                typePay: typePay,
                folioPay: folioPay,
                estadoPay: estadoPay,
                montoExtra: montoExtra,
                user_id:user_id,
                msisdn:msisdn,
                dealer_id:dealer_id
            };

            $.ajax({
                url: "{{URL::route('altcel2.save.monthly')}}",
                data: data,
                beforeSend: function(){
                    Swal.fire({
                        title: 'Realizando operación...',
                        html: 'Espera un poco, un poquito más...',
                        showConfirmButton: false,
                        didOpen: () => {
                            Swal.showLoading();
                        }
                    });
                },
                success: function(response){
                    if(response == 1){
                        Swal.fire({
                            icon: 'success',
                            title: 'Hecho',
                            showConfirmButton: false,
                            timer: 2000
                        });

                        $.ajax({
                            url: "{{URL::route('altcel2.unbarring')}}",
                            data: {payID:payID},
                            beforeSend: function(){
                                Swal.fire({
                                    title: 'Verificando estado del servicio, en caso de estar suspendido se reanudará.',
                                    html: 'Espera un poco, un poquito más...',
                                    showConfirmButton: false,
                                    didOpen: () => {
                                        Swal.showLoading();
                                    }
                                });
                            },
                            success:function(response){
                                console.log(response);
                                if(response == 1){
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'El servicio ha sido reanudado.',
                                        showConfirmButton: false,
                                    });
                                    setTimeout(function(){ location.reload();},3000);
                                }else{
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Hubo un problema al reanudar el servicio, consulte a Desarrollo.',
                                        text: 'Bad Request'
                                    });
                                }
                            }
                        });
                    }else if(response == 2){
                        Swal.fire({
                            icon: 'error',
                            title: 'Woops!',
                            text: 'Al parecer no cuestas con saldo suficiente, actualizando la ventana.'
                        });
                        setTimeout(function(){ location.reload();},3000);
                    }else{
                        Swal.fire({
                            icon: 'errpr',
                            title: 'Ha ocurrido un error, consulte a Desarrollo.',
                            showConfirmButton: false,
                            timer: 2000
                        });
                    }
                }
            });
        }else{
            let producto = $('#product').val();
            let lat = $('#lat').val();
            let lng = $('#lng').val();
            let rate = $('#ratesSelect').val();
            let offerID = $('#ratesSelect option:selected').attr('data-offerid');
            let offer_id = $('#ratesSelect option:selected').attr('data-offer-id');
            let type = "internalExternalChange";
            let comment = '', reason = 'cobro', status = 'completado', scheduleDate = '', address;

            if(producto == 'HBB'){
                address = lat+','+lng;
            }else{
                address = null;
            }

            data = {
                msisdn:msisdn, 
                rate_id:rate, 
                offerID:offerID, 
                offer_id:offer_id, 
                type:type, 
                scheduleDate:scheduleDate, 
                address:address,
                producto:producto,
                user_id:user_id,
                amount:monto,
                service: service,
                payID: payID,
                monto: monto,
                typePay: typePay,
                folioPay: folioPay,
                estadoPay: estadoPay,
                montoExtra: montoExtra,
                comment:comment,
                reason:reason,
                status:status,
                dealer_id:dealer_id
            }

            // console.log(data);
            // return false;
            $.ajax({
                url: "{{URL::route('altcel2.change.product')}}",
                data: data,
                beforeSend: function(){
                    Swal.fire({
                        title: 'Realizando operación...',
                        html: 'Espera un poco, un poquito más...',
                        showConfirmButton: false,
                        didOpen: () => {
                            Swal.showLoading();
                        }
                    });
                },
                success: function(response){
                    if(response.http_code == 1 || response.http_code == 2){
                        Swal.fire({
                            icon: 'success',
                            title: 'Hecho',
                            showConfirmButton: false,
                            timer: 2000
                        });
                        setTimeout(function(){ location.reload();},3000);
                    }else if(response.http_code == 3){
                        Swal.fire({
                            icon: 'error',
                            title: 'Woops!',
                            text: response.message
                        });
                        setTimeout(function(){ location.reload();},3000);
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Woops!',
                            text: response.message
                        });
                    }
                }
            });
        }
    }

    $('#msisdn, #msisdn2').keyup(function () {
        if ($('#msisdn').val() == $('#msisdn2').val()) {
            // Validación de existencia de SIM
            let msisdn = $('#msisdn').val();

            $.ajax({
                url: "{{URL::route('altcel2.verify.exists.msisdn')}}",
                data: {msisdn:msisdn},
                beforeSend: function(){
                    Swal.fire({
                        title: 'Verificando número...',
                        showConfirmButton: false,
                        didOpen: () => {
                            Swal.showLoading();
                        }
                    });
                },
                success: function(response){
                    Swal.close();
                    if(response.http_code == 1){
                        $('.movements').attr('disabled',false);
                        $('.movements').removeClass('d-none');
                        if(response.product == 'MOV'){
                            $('#monthly').attr('disabled',true);
                            $('#change').attr('disabled',true);
                            $('#surplus').attr('disabled',false);

                            $('#monthly').addClass('d-none');
                            $('#change').addClass('d-none');
                            $('#surplus').removeClass('d-none');
                        }
                        
                    }else if(response.http_code == 2){
                        $('#monthly').attr('disabled',false);
                        // $('#change').attr('disabled',false);
                        $('#surplus').attr('disabled',true);

                        $('#monthly').removeClass('d-none');
                        // $('#change').removeClass('d-none');
                        $('#surplus').addClass('d-none');
                    }else{
                        $('.movements').attr('disabled',true);
                        Swal.fire({
                            icon: 'error',
                            title: 'Woops!',
                            text: response.message,
                            showConfirmButton: false,
                            timer: 3000
                        });
                        return false;
                    }
                }
            });
        }else {
            $('.movements').attr('disabled',true);
            $('.movements').addClass('d-none');
            $('#execute').attr('disabled',true);
            $('#ratesSelectContent').addClass('d-none');
        }
        
    });
</script>

@endsection