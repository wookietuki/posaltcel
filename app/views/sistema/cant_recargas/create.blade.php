@extends('dashboard.layouts.dashboard.master')


{{ HTML::style('css/form.css') }}

{{ HTML::script('js/form.css') }}

@section('content')
@include('notifications')
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-danger">
				<div class="panel-body">
					<h3>
            <?php $icon = end($breadcrumb)['icon']; ?>
            <i class="<?php echo (strpos($icon,'fa-')!==false)?'fa':'glyphicon '; ?> {{$icon}}"></i>
            {{end($breadcrumb)['title']}}
					</h3>
					<hr>
					<?php
						if (isset($cant_recarga)) {
							$method = 'PUT';
							$route = ['catalogo.cant_recarga.update',$cant_recarga->id];
						} else {
							$method = 'POST';
							$route = 'catalogo.cant_recarga.store';
						}
					?>
					{{ Form::open(['id'=>'form','method'=>$method,'route'=>$route,'role'=>'form']) }}

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Monto</label>
			              {{ Form::text('monto', Input::old('monto', isset($cant_recarga)?$cant_recarga->monto:null), ['class'=>'form-control', 'required', 'placeholder'=>'Monto']) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">SKU</label>
			              {{ Form::text('sku', Input::old('sku', isset($cant_recarga)?$cant_recarga->sku:null), ['class'=>'form-control', 'required', 'placeholder'=>'SKU']) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
			              Visible <br>
                    	{{ Form::checkbox('visible', Input::old('visible', isset($cant_recarga)?$cant_recarga->visible:null), isset($cant_recarga)?$cant_recarga->visible==1?'ckecked':null:null, ['class'=>'form-control flat-red']) }}
			            </div>					
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									{{ Form::submit('Guardar', array('class'=>'btn btn-primary')) }}
								</div>
							</div>
						</div>
	
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script type="text/javascript">
  	
  	$("#form").validate();

  </script>
@endsection