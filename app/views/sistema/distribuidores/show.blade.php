@extends('dashboard.layouts.dashboard.master')

@section('content')

@include('notifications')

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Distribuidor: <b><u>{{ $distribuidor->user->first_name }} {{ $distribuidor->user->last_name }}</u></b></h3>
		</div>
		<div class="panel-body">
			<div class="table table-responsive">
				<table class="table clickable">
					<thead>
						<tr>
							<th nowrap>Fecha</th>
							<th nowrap>Folio</th>
							<th nowrap>Compañia</th>
							<th nowrap>Cantidad</th>
							<th nowrap>Comprobante</th>
							<th nowrap>Validado</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($tickets as $ticket)
							<tr>
								<td>{{ $ticket->fecha }}</td>
								<td>{{ $ticket->folio }}</td>
								<td>{{ $ticket->compania }}</td>
								<td class="text-right">$ {{ number_format($ticket->cantidad,2,'.',',') }}</td>
								<td class="text-center">
									<a href="#" id="myImg{{ $ticket->id }}"><i class="fas fa-receipt fa-2x"></i></a>
								</td>
								<td class="text-center">
									@if ($ticket->verificado == 0)
										@if($ticket->compania == 'Conecta')
											<button class="btn btn-success verifyConecta" data-amount="{{$ticket->cantidad}}" data-distribuidor="{{$distribuidor->id}}" data-ticket="{{$ticket->id}}">Verificar</button>
										@elseif($ticket->compania == 'Altcel' || $ticket->compania == null)
											{{ Form::open(['route'=>['ticket.update',$ticket->id,'dist'=>$distribuidor->id],'id'=>'myForm'.$ticket->id,'method'=>'PUT']) }}

												<button type="submit" class="btn btn-success">Verificar</button>

											{{ Form::close() }}
										@endif

										
										
									@else
										<i class="fas fa-check fa-2x" style="color: green;"></i>
									@endif
								</td>
							</tr>

							<!-- The Modal -->
              <div id="myModal{{ $ticket->id }}" class="modal">

                <!-- The Close Button -->
                <span class="close" id="close{{ $ticket->id }}"> <i class="fas fa-times fa-2x"></i> </span>

                <!-- Modal Content (The Image) -->
                <img class="modal-content" id="img01{{ $ticket->id }}" width="70%">

                <!-- Modal Caption (Image Text) -->
                <div id="caption"></div>
              </div>

							<script type="text/javascript">
                // Get the modal
                var modal = document.getElementById('myModal{{ $ticket->id }}');

                // Get the image and insert it inside the modal - use its "alt" text as a caption
                var img = document.getElementById('myImg{{ $ticket->id }}');
                var modalImg = document.getElementById("img01{{ $ticket->id }}");
                img.onclick = function(){
                  modal.style.display = "block";
                  modalImg.src = '{{ asset($ticket->foto_ticket) }}';
                }

                // Get the <span> element that closes the modal
                var span = document.getElementById("close{{ $ticket->id }}");

                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                modal.style.display = "none";
                }
              </script>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
<script>
	$('.verifyConecta').click(function(){
		let amount = $(this).data('amount');
		let distribuidor = $(this).data('distribuidor');
		let ticket = $(this).data('ticket');
		
		$.ajax({
			url: "{{URL::route('ticket.update.altcel2')}}",
			data:{dealer:distribuidor,amount:amount,ticket:ticket},
			beforeSend: function(){
				Swal.fire({
					title: 'Actualizando datos...',
					html: 'Espera un poco, un poquito más...',
					showConfirmButton: false,
					didOpen: () => {
						Swal.showLoading();
					}
				});
			},
			success:function(response){
				if(response.http_code == 1){
					Swal.fire({
                        icon: 'success',
                        title: 'Listo',
                        text: 'Ticket verificado.'
                    });
					setTimeout(function(){ location.reload();},1200);
				}else{
					Swal.fire({
                        icon: 'error',
                        title: 'Woops!',
                        text: 'Algo salió mal, consulte a Desarrollo.'
                    });
				}
			}
		});
	});
</script>

@stop