@extends('dashboard.layouts.dashboard.master')

@section('content')

@include('notifications')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-danger">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<a href="{{ URL::route('distribuidor.create') }}"><i class="fas fa-plus"></i> Agregar Distribuidor</a>
						</div>
					</div>
					<br>
					<table class="table" id="distribuidores">
					  <thead>
					  	<tr>
					  		<th>Nombre</th>
					  		<th>Distribuidor</th>
					  		<th>Correo</th>
					  		<th>Teléfono</th>
					  		<th>Acciones</th>
					  	</tr>
					  </thead>
					  <tbody>
					  	@foreach ($distribuidores as $distribuidor)
						  	<tr class="text-center">
						  		<td>{{ $distribuidor->user->last_name }} {{ $distribuidor->user->first_name }}</td>
						  		<td>{{ $distribuidor->distribuidor }}</td>
						  		<td>{{ $distribuidor->user->email }}</td>
						  		<td>{{ $distribuidor->telefono }}</td>
						  		<td>
								  	<div class="btn-group" role="group" aria-label="acciones">
								  		<a href="{{ URL::route('distribuidor.show', $distribuidor->id) }}" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> Info</a>
										<a class="btn btn-sm btn-info" href="{{ URL::route('distribuidor.edit', $distribuidor->id) }}" data-toggle="tooltip" data-placement="top" title="Editar Distribuidor"><i class="fas fa-pencil-alt"></i> Editar</a>
										<a class="btn btn-sm btn-danger" href="#" data-placement="top" title="Borrar Distribuidor" data-toggle="modal" data-target=".bs-example-modal-lg{{ $distribuidor->id }}"><i class="fas fa-trash"></i> Borrar</a>
						  		</td>
						  	</tr>
						  		<div class="modal fade bs-example-modal-lg{{ $distribuidor->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
								  <div class="modal-dialog modal-lg">
								    <div class="modal-content">

								      <div class="modal-body">

								      <h1>¡Atención!</h1>
								      <h3>Se va a eliminar al Distribuidor: <b>{{ $distribuidor->user->last_name }} {{ $distribuidor->user->first_name }}</b>.</h3>
								      <h3>¿Está seguro?</h3>
								      {{ Form::open(['route'=>['distribuidor.destroy',$distribuidor->id],'id'=>'myForm'.$distribuidor->id,'method'=>'DELETE']) }}
								        <a href="#" onclick="document.getElementById('myForm{{ $distribuidor->id }}').submit();" class="btn btn-danger btn-lg"><b><i class="fas fa-trash fa-2x"></i> Borrar</b></a>
								        <button type="button" class="btn btn-default btn-lg pull-right" data-dismiss="modal"><b><i class="fa fa-thumbs-up fa-2x"></i> Cancelar</b></button>
								      {{ Form::close() }}
								      </div>
								    </div>
								  </div>
								</div>
					  	@endforeach
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@stop

@section('scripts')
	
	<!-- page script -->
  {{ HTML::script('//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js') }}
  {{ HTML::script('//cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.bootstrap.min.js') }}
  {{ HTML::script('//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') }}
  {{ HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js') }}
  {{ HTML::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js') }}
  {{ HTML::script('//cdn.datatables.net/buttons/1.4.0/js/buttons.colVis.min.js') }}

  <!-- page script -->
  <script type="text/javascript">
    $(function() {
      $('#distribuidores').dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "sDom": '<"top"Bif>rt<"bottom"pl><"clear">',
        "sButtons": [
          'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "bAutoWidth": true,
        "oLanguage": {
        "sLengthMenu": "_MENU_ distribuidores por página",
        "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_ distribuidores",
        "sEmptyTable": "No se encontraron datos en la tabla",
        "sInfoEmpty": "Mostrando del 0 al 0 de 0 distribuidores",
        "sInfoFiltered": "(filtrado de un total de _MAX_ distribuidores)",
        "sLoadingRecords": "Cargando...",
        "sProcessing": "Procesando...",
        "sSearch": "Buscar:",
        "sZeroRecords": "No se encontraron registros con la búsqueda",
        "oPaginate": {
          "sNext": "Siguiente",
          "sPrevious": "Anterior",
        }
        },
      });
    });
  </script>

@stop