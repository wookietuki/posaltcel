@extends('dashboard.layouts.dashboard.master')


{{ HTML::style('css/form.css') }}

{{ HTML::script('js/form.css') }}

@section('content')
@include('notifications')
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-danger">
				<div class="panel-body">
					<h3>
            <?php $icon = end($breadcrumb)['icon']; ?>
            <i class="<?php echo (strpos($icon,'fa-')!==false)?'fa':'glyphicon '; ?> {{$icon}}"></i>
            {{end($breadcrumb)['title']}}
					</h3>
					<hr>
					<?php
						if (isset($distribuidor)) {
							$method = 'PUT';
							$route = ['distribuidor.update',$distribuidor->id];
						} else {
							$method = 'POST';
							$route = 'distribuidor.store';
						}
					?>
					{{ Form::open(['id'=>'form','method'=>$method,'route'=>$route,'role'=>'form']) }}

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Nombre</label>
			              {{ Form::text('first_name', Input::old('first_name', isset($distribuidor)?$distribuidor->user->first_name:null), ['class'=>'form-control', 'required', 'placeholder'=>'Nombre']) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Apellido</label>
			              {{ Form::text('last_name', Input::old('last_name', isset($distribuidor)?$distribuidor->user->last_name:null), ['class'=>'form-control', 'required', 'placeholder'=>'Apellido']) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Distribuidor</label>
			              {{ Form::text('distribuidor', Input::old('distribuidor', isset($distribuidor)?$distribuidor->distribuidor:null), ['class'=>'form-control', 'required', 'placeholder'=>'Distribuidor']) }}
			            </div>					
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Teléfono</label>
			              {{ Form::text('telefono', Input::old('telefono', isset($distribuidor)?$distribuidor->telefono:null), ['class'=>'form-control', 'required', 'placeholder'=>'Teléfono']) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Usuario</label>
			              {{ Form::text('username', Input::old('username', isset($distribuidor)?$distribuidor->user->username:null), ['class'=>'form-control', 'required', 'placeholder'=>'Usuario',isset($distribuidor)?'Readonly':'']) }}
			            </div>					
								</div>

								<div class="col-md-4">
									<div class="form-group float-label-control">
			              <label for="">Correo Electrónico</label>
			              {{ Form::email('email', Input::old('email', isset($distribuidor)?$distribuidor->user->email:null), ['class'=>'form-control', 'required', 'placeholder'=>'Correo Electrónico',isset($distribuidor)?'Readonly':'']) }}
			            </div>					
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-3">
									<div class="form-group float-label-control">
			              <label for="">ID de Grupo</label>
			              {{ Form::text('id_grp', Input::old('id_grp', isset($distribuidor)?$distribuidor->id_grp:null), ['class'=>'form-control', 'required', 'placeholder'=>'ID de Grupo']) }}
			            </div>					
								</div>

								<div class="col-md-3">
									<div class="form-group float-label-control">
			              <label for="">ID de Cadena</label>
			              {{ Form::text('id_chain', Input::old('id_chain', isset($distribuidor)?$distribuidor->id_chain:null), ['class'=>'form-control', 'required', 'placeholder'=>'ID de Cadena']) }}
			            </div>					
								</div>

								<div class="col-md-3">
									<div class="form-group float-label-control">
			              <label for="">ID de Tienda</label>
			              {{ Form::text('id_merchant', Input::old('id_merchant', isset($distribuidor)?$distribuidor->id_merchant:null), ['class'=>'form-control', 'required', 'placeholder'=>'ID de Tienda']) }}
			            </div>					
								</div>

								<div class="col-md-3">
									<div class="form-group float-label-control">
			              <label for="">ID de POS</label>
			              {{ Form::text('id_pos', Input::old('id_pos', isset($distribuidor)?$distribuidor->id_pos:null), ['class'=>'form-control', 'required', 'placeholder'=>'ID de POS']) }}
			            </div>					
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group float-label-control">
			              <label for="">URL de Tienda</label>
			              {{ Form::text('url', Input::old('url', isset($distribuidor)?$distribuidor->url:null), ['class'=>'form-control', 'required', 'placeholder'=>'URL de Tienda']) }}
			            </div>					
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group float-label-control">
			              <label for="">Observaciones</label>
			              {{ Form::text('observaciones', Input::old('observaciones', isset($distribuidor)?$distribuidor->observaciones:null), ['class'=>'form-control', 'placeholder'=>'Observaciones']) }}
			            </div>					
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									{{ Form::submit('Guardar', array('class'=>'btn btn-primary')) }}
								</div>
							</div>
						</div>
	
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
@stop

@section('scripts')
	<script type="text/javascript">
  	
  	$("#form").validate();

  </script>
@endsection