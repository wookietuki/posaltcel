@extends(Config::get('syntara::views.master'))

{{ HTML::style('css/form.css') }}



@section('content')

@include('notifications')

@if (Session::has('popup'))


  <script>
    var myWindow = window.open("{{ URL::to('recarga', Session::get('popup')) }}", "", "width=800,height=800");
  </script>
@endif

  @if (Sentry::check())

    <div class="panel panel-danger">
      <div class="panel-body">
  
          <div class="row">

            @if ($user->inGroup($groupDist))

            <?php
              $url = $userDist->url.'ServicePX.asmx/SaldoDisponible?lGrupo='.$userDist->id_grp.'&lCadena='.$userDist->id_chain.'&lTienda='.$userDist->id_merchant;


              $client = new \GuzzleHttp\Client(); 
              try {
                $response = $client->request(
                  'GET', 
                  (($url)), 
                  [ 
                    'headers' => [
                      'Content-Type' => 'text/xml; charset=UTF8',
                      'Accept' => 'application/xml'
                    ], 
                    'timeout' => 50
                  ])->getBody()->getContents(); 
              } catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
              }

              function get_string_between($string, $start, $end){
                $string = ' ' . $string;
                $ini = strpos($string, $start);
                if ($ini == 0) return '';
                $ini += strlen($start);
                $len = strpos($string, $end, $ini) - $ini;
                return substr($string, $ini, $len);
              }

              if(isset($response)){
                $saldo = get_string_between(htmlspecialchars_decode($response), '<double xmlns="http://www.pagoexpress.com.mx/ServicePX">', '</double>');
              } else {
                $saldo = '0';
              }
            ?>

              <div class="col-lg-4 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-green">
                  <div class="inner">
                    <h3>
                      $ {{ number_format($saldo,2,'.',',') }}
                    </h3>
                    <p>
                      SALDO
                    </p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-cash-register"></i>
                  </div>
                  <a href="{{ URL::route('recarga.index') }}" class="small-box-footer">
                    Ver <i class="fa fa-arrow-circle-right"></i>
                  </a>
                </div>
              </div><!-- ./col -->
            @endif
         
          </div>

      </div>
    </div>

    @if ($user->inGroup($groupDist))
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-danger">
          <div class="panel-heading">
            <h3 class="panel-title text-center">Recargas <img src="{{ asset('images/logo1.png') }}" width="80px" alt="Altcel"></h3>
          </div>
          <div class="panel-body">
            <div class="row">
              {{ Form::open(['id'=>'form','method'=>'POST','route'=>'recarga.store','role'=>'form','autocomplete'=>'off']) }}
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4">
                      <div class="form-group float-label-control">
                        <label for="">Número de Teléfono</label>

                        <input type="tel" name="telefono" value="{{ Input::old('telefono', isset($recarga)?$recarga->telefono:null) }}" class="form-control" required placeholder="Número de Teléfono" id="telefono">
                      </div>          
                    </div>

                    <div class="col-md-4">
                      <div class="form-group float-label-control">
                        <label for="">Confirma tu Número</label>
                        <input type="tel" name="tel_conf" value="{{ Input::old('tel_conf', isset($recarga)?$recarga->tel_conf:null) }}" class="form-control" required placeholder="Confirma tu Número" id="tel_conf">

                      </div>          
                    </div>

                    <div class="col-md-4">
                      <div class="form-group float-label-control">
                        <label for="">Monto</label>
                        {{ Form::select('monto', [null=>'Seleccione uno']+$montos, Input::old('monto', isset($recarga)?$recarga->monto:null), array('class'=>'form-control', 'required')) }}
                      </div>          
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      {{ Form::submit('Hacer Recarga', array('class'=>'btn btn-primary btn-bg','onClick'=>'DisableButton(this)','id'=>'SubmitButton')) }}
                    </div>
                  </div>
                </div>
              {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>
    @else
      <div class="col-md-12">
        <!-- LINE CHART -->
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title">Recargas Diarias</h3>
          </div>
          <div class="box-body chart-responsive">
            <div class="chart" id="line-chart" style="height: 300px;"></div>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col (RIGHT) -->
    @endif

  @endif
@stop

@section('scripts')

  <script>
    function DisableButton(b)
    {
      /*b.disabled = true;
      b.value = 'Haciendo Recarga...';
      b.form.submit();*/

    }
  </script>

  <script type="text/javascript">

    $("#form").validate({
      rules: {
        telefono: {
          digits: true,
          minlength: 10,
          maxlength:10
        },
        tel_conf:{
          equalTo:"#telefono"
        },
      },
      success: function(label) {
        label.addClass("valid").text("Ok!")
      },
      submitHandler: function() { 
        //alert("Submitted!")
        $('#SubmitButton').prop('disabled', 'disabled');
        form.submit(); 
      }
    });

    $(function() {
        startTime();
        $(".center").center();
        $(window).resize(function() {
            $(".center").center();
        });
    });

    /*  */
    function startTime()
    {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();

        // add a zero in front of numbers<10
        m = checkTime(m);
        s = checkTime(s);

        //Check for PM and AM
        var day_or_night = (h > 11) ? "PM" : "AM";

        //Convert to 12 hours system
        if (h > 12)
            h -= 12;

        //Add time to the headline and update every 500 milliseconds
        $('#time').html(h + ":" + m + ":" + s + " " + day_or_night);
        setTimeout(function() {
            startTime()
        }, 500);
    }

    function checkTime(i)
    {
        if (i < 10)
        {
            i = "0" + i;
        }
        return i;
    }

    // LINE CHART
    var line = new Morris.Area({
      parseTime:false,
      element: 'line-chart',
      resize: true,
      data: [
        @foreach ($totales as $total)
          {y: '{{ $total->fecha }}', item1: {{ $total->total }}},
        @endforeach
      ],
      xkey: 'y',
      ykeys: ['item1'],
      labels: ['Recargas'],
      lineColors: ['#0CAAE0'],
      hideHover: 'auto',
      behaveLikeLine: true,
      pointFillColors:['#ffffff'],
      pointStrokeColors: ['black'],
    });

  </script>
  {{ HTML::script('js/form.js') }}
@endsection