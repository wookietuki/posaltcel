@extends(Config::get('syntara::views.master'))

{{ HTML::style('css/form.css') }}



@section('content')

@include('notifications')

  <div class="col-md-6">

    <a href="{{ URL::to('recargaAltcel') }}">
      <div class="panel panel-danger">
        <div class="panel-body text-center">
          <img src="{{ asset('images/Recarga_Altcel.png') }}" width="250px" alt="">
        </div>
      </div>
    </a>

  </div>
  <div class="col-md-6">

    <a href="{{ URL::to('recargaConecta') }}">
      <div class="panel panel-danger">
        <div class="panel-body text-center">
          <img src="{{ asset('images/Recarga_conecta.png') }}" width="250px" alt="">
        </div>
      </div>
    </a>

  </div>

@stop