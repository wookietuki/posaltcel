@extends('dashboard.layouts.dashboard.master')

{{ HTML::style('css/form.css') }}

{{ HTML::script('js/form.css') }}

@section('content')
@include('notifications')
<h1>Portabilidad</h1>
<div class="container">

  @if (Session::has('error'))
  <div class="alert alert-danger alert-dismissable">
    <i class="fas fa-ban"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <b><i class="fas fa-times"></i></b> {{ Session::get('error') }}
  </div>
  @endif

  @if (Session::has('success'))
    <div class="alert alert-success alert-dismissable">
      <i class="fas fa-check"></i>
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <b><i class="fas fa-thumbs-up"></i></b> {{ Session::get('success') }}
    </div>
  @endif
  
  {{ Form::open(['id'=>'form','method'=>'POST','route'=>'venta_sim.actualizar','role'=>'form','autocomplete'=>'off']) }}
<div class="row">
  <div class="col-md-3 mb-3">
      <label for="validationServer013">Nombre y Apellidos</label>
      <input type="text" value="{{ Input::old('nombre') }}" class="form-control" id="validationServer013" placeholder="Nombre y Apellidos" name="nombre" required>
  </div>

  <div class="col-md-3 mb-3">
      <label for="validationServer013">CURP</label>
      <input type="text" value="{{ Input::old('curp') }}" class="form-control" id="validationServer013" placeholder="CURP" name="curp" required>
  </div>

  {{-- <div class="col-md-3 mb-3">
      <label for="validationServer013">Número Telefonico</label>
      <input type="text" value="{{ Input::old('numero') }}" class="form-control" id="validationServer013" placeholder="Número Telefonico" name="numero" required>
  </div>

  <div class="col-md-3 mb-3">
      <label for="validationServer013">NIP</label>
      <input type="text" value="{{ Input::old('nip') }}" class="form-control" id="validationServer013" placeholder="NIP" name="nip" required>
  </div> --}}
  
</div>
<div class="row">
  {{-- <div class="col-md-3 mb-3">
      <label for="validationServer013">Fecha de Portabilidad</label>
      <input type="date" value="{{ Input::old('fecha') }}" class="form-control" id="validationServer013" placeholder="Fecha de Portabilidad" name="fecha" required>
  </div> --}}

  <div class="col-md-3 mb-3">
      <label for="validationServer013">SIM</label>
      <input type="text" value="{{ Input::old('iccid') }}" class="form-control" id="validationServer013" placeholder="SIM" name="iccid" required>
  </div>

  <div class="col-md-3 mb-3">
      <label for="validationServer013">INE</label>
      <input type="text" value="{{ Input::old('ine') }}" class="form-control" id="validationServer013" placeholder="INE" name="ine" required>
  </div>

  <div class="col-md-3 mb-3">
      <label for="validationServer013">Dirección</label>
      <input type="text" value="{{ Input::old('direccion') }}" class="form-control" id="validationServer013" placeholder="Dirección" name="direccion" required>
  </div>
</div>
<div class="row">
  {{-- <div class="col-md-3 mb-3">
      <label for="validationServer013">Compañia</label>
      <!--<input type="text" value="{{ Input::old('') }}" class="form-control" id="validationServer013" placeholder="Compañia" name="cia" required>-->
      <select class="form-control" id="inputGroupSelect01" name="cia" required>
          <option {{ (Input::old("cia") == 'Compañía Anterior' ? "selected":"") }} value="0"> Compañía Anterior</option>
          <option {{ (Input::old("cia") == 'ALO' ? "selected":"") }} value="ALO">ALO</option>
          <option {{ (Input::old("cia") == 'AT&T' ? "selected":"") }} value="AT&T">AT&T</option>
          <option {{ (Input::old("cia") == 'BUENOCELL' ? "selected":"") }} value="BUENOCELL">BUENOCELL</option>
          <option {{ (Input::old("cia") == 'CIERTO' ? "selected":"") }} value="CIERTO">CIERTO</option>
          <option {{ (Input::old("cia") == 'FLASH MOBILE' ? "selected":"") }} value="FLASH MOBILE">FLASH MOBILE</option>
          <option {{ (Input::old("cia") == 'FREEDOMPOP' ? "selected":"") }} value="FREEDOMPOP">FREEDOMPOP</option>
          <option {{ (Input::old("cia") == 'HER MOBILE' ? "selected":"") }} value="HER MOBILE">HER MOBILE</option>
          <option {{ (Input::old("cia") == 'MAZ TIEMPO' ? "selected":"") }} value="MAZ TIEMPO">MAZ TIEMPO</option>
          <option {{ (Input::old("cia") == 'MOVISTAR' ? "selected":"") }} value="MOVISTAR">MOVISTAR</option>
          <option {{ (Input::old("cia") == 'OUI' ? "selected":"") }} value="OUI">OUI</option>
          <option {{ (Input::old("cia") == 'QBOCEL' ? "selected":"") }} value="QBOCEL">QBOCEL</option>
          <option {{ (Input::old("cia") == 'SIMPATI' ? "selected":"") }} value="SIMPATI">SIMPATI</option>
          <option {{ (Input::old("cia") == 'SIMPLII' ? "selected":"") }} value="SIMPLII">SIMPLII</option>
          <option {{ (Input::old("cia") == 'SORIANA' ? "selected":"") }} value="SORIANA">SORIANA</option>
          <option {{ (Input::old("cia") == 'TELCEL' ? "selected":"") }} value="TELCEL">TELCEL</option>
          <option {{ (Input::old("cia") == 'TOKAMÓVIL' ? "selected":"") }} value="TOKAMÓVIL">TOKAMÓVIL</option>
          <option {{ (Input::old("cia") == 'UNEFON' ? "selected":"") }} value="UNEFON">UNEFON</option>
          <option {{ (Input::old("cia") == 'VIRGIN MOBILE' ? "selected":"") }} value="VIRGIN MOBILE">VIRGIN MOBILE</option>
          <option {{ (Input::old("cia") == 'WEEX' ? "selected":"") }} value="WEEX">WEEX</option>
      </select>
  </div> --}}
  <div class="col-md-3 mb-3">
      {{-- <label for="validationServer013">Promoción</label>
      <select id="promo" name="promo" class="form-control" required>
          <option {{ (Input::old("promo") == 'Seleccione Promo' ? "selected":"") }} value="0"> Seleccione Promo</option>
          <option {{ (Input::old("promo")=='Altcel 40 - Escala Recarga'?"selected":"") }} value="Altcel 40 - Escala Recarga">Altcel 40 - Escala Recarga</option>
          <option {{ (Input::old("promo") == 'Altcel Ilimitado' ? "selected":"") }} value="Altcel Ilimitado">Altcel Ilimitado</option>
          <option {{ (Input::old("promo") == 'Altcel 80 Saldo Regalo 3 meses' ? "selected":"") }} value="Altcel 80 Saldo Regalo 3 meses">Altcel 80 Saldo Regalo 3 meses</option>
      </select> --}}
  </div>
</div>
<br>
<div class="row">
  <button class="btn btn-primary" type="submit">Guardar</button>
</div>
</form></div>
@endsection