@extends('dashboard.layouts.dashboard.master')

{{ HTML::style('css/form.css') }}

{{ HTML::script('js/form.css') }}

@section('content')
@include('notifications')

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">SIM Portabilidad</div>
            <div class="panel-body text-center">
                <a href="{{ URL::route('venta_sim.create') }}">
                    <i class="fas fa-sim-card fa-3x"></i><br> Formulario Portabilidad
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">SIM Linea Nueva</div>
            <div class="panel-body text-center">
                <a href="{{ URL::route('venta_sim.edit') }}">
                    <i class="fas fa-sim-card fa-3x"></i><br> Formulario Linea Nueva
                </a>
            </div>
        </div>
    </div>
</div>

@stop