@extends(Config::get('syntara::views.master'))

{{ HTML::style('css/form.css') }}



@section('content')

@include('notifications')

@if (Session::has('popup'))


  <script>
    var myWindow = window.open("{{ URL::to('recarga', Session::get('popup')) }}", "", "width=800,height=800");
  </script>
@endif

  @if (Sentry::check())

    <div class="panel panel-danger">
      <div class="panel-body">
  
          <div class="row">

            @if ($currentUser->hasAccess('view-distribuidor-list'))

              <div class="col-lg-4 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-aqua">
                  <div class="inner">
                    <h3>
                      {{ $distribuidores }}
                    </h3>
                    <p>
                      Distribuidores
                    </p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-cash-register"></i>
                  </div>
                  <a href="{{ URL::route('distribuidor.index') }}" class="small-box-footer">
                    Ver <i class="fa fa-arrow-circle-right"></i>
                  </a>
                </div>
              </div><!-- ./col -->

            @endif
              
          </div>

      </div>
    </div>

    @if ($user->inGroup($groupDist))
      <div class="row">
        <div class="col-md-4">
          <a href="{{ URL::route('altcel.index') }}">
            <div class="panel panel-danger">
              <div class="panel-body text-center">
                <img src="{{ asset('images/Recarga_Altcel.png') }}" width="150px" alt="">
              </div>
            </div>
          </a>

        </div>
        <div class="col-md-4">
          <a href="{{ URL::route('altcel2.conecta.index')}}">
            <div class="panel panel-danger">
              <div class="panel-body text-center">
                <img src="{{ asset('images/Recarga_conecta.png') }}" width="150px" alt="">
              </div>
            </div>
          </a>
        </div>
        <div class="col-md-4">
            <a href="{{ URL::route('venta_sim.index')}}">
              <div class="panel panel-danger">
                <div class="panel-body text-center">
                    <img src="{{ asset('images/Ventas_Sim.png') }}" width="150px" alt="">
                </div>
              </div>
            </a>
          </div>
        <!-- {{ Sentry::getUser()->username }} -->
      </div>
    @else
      <div class="col-md-12">
        <!-- LINE CHART -->
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title">Recargas Diarias</h3>
          </div>
          <div class="box-body chart-responsive">
            <div class="chart" id="line-chart" style="height: 300px;"></div>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col (RIGHT) -->
    @endif

  @endif
@stop

@section('scripts')

  <script>
    function DisableButton(b)
    {
      /*b.disabled = true;
      b.value = 'Haciendo Recarga...';
      b.form.submit();*/

    }
  </script>

  <script type="text/javascript">

    $("#form").validate({
      rules: {
        telefono: {
          digits: true,
          minlength: 10,
          maxlength:10
        },
        tel_conf:{
          equalTo:"#telefono"
        },
      },
      success: function(label) {
        label.addClass("valid").text("Ok!")
      },
      submitHandler: function() {
        //alert("Submitted!")
        $('#SubmitButton').prop('disabled', 'disabled');
        form.submit();
      }
    });

    $(function() {
        startTime();
        $(".center").center();
        $(window).resize(function() {
            $(".center").center();
        });
    });

    /*  */
    function startTime()
    {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();

        // add a zero in front of numbers<10
        m = checkTime(m);
        s = checkTime(s);

        //Check for PM and AM
        var day_or_night = (h > 11) ? "PM" : "AM";

        //Convert to 12 hours system
        if (h > 12)
            h -= 12;

        //Add time to the headline and update every 500 milliseconds
        $('#time').html(h + ":" + m + ":" + s + " " + day_or_night);
        setTimeout(function() {
            startTime()
        }, 500);
    }

    function checkTime(i)
    {
        if (i < 10)
        {
            i = "0" + i;
        }
        return i;
    }

    // LINE CHART
    var line = new Morris.Area({
      parseTime:false,
      element: 'line-chart',
      resize: true,
      data: [
        @foreach ($totales as $total)
          {y: '{{ $total->fecha }}', item1: {{ $total->total }}},
        @endforeach
      ],
      xkey: 'y',
      ykeys: ['item1'],
      labels: ['Recargas'],
      lineColors: ['#0CAAE0'],
      hideHover: 'auto',
      behaveLikeLine: true,
      pointFillColors:['#ffffff'],
      pointStrokeColors: ['black'],
    });

  </script>
  {{ HTML::script('js/form.js') }}
@endsection