@if (Sentry::check())

	@if ($currentUser->hasAccess('catalogs-management'))
		<li class="treeview">
			<a href="#">
				<i class="fas fa-cogs"></i> <span>Configuración</span>
				<i class="fas fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
				<li class="treeview" id="Menu1">
					<a href="#">
						<i class="fas fa-book"></i> <span>Catálogos</span>
						<i class="fas fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu" id="Menu1-1">
						@if ($currentUser->hasAccess('view-cant_recarga-list'))
							<li><a href="{{ URL::route('catalogo.cant_recarga.index') }}"><i class="fas fa-file-invoice-dollar"></i> Cant. de Recarga</a></li>
						@endif
					</ul>
				</li>
			</ul>
		</li>
	@endif

	@if ($currentUser->hasAccess('view-distribuidor-list'))

		<li><a href="{{ URL::route('distribuidor.index') }}"><i class="fas fa-cash-register"></i> Distribuidores</a></li>

	@endif

	@if ($currentUser->hasAccess('view-recarga-list'))
		
		<li><a href="{{ URL::route('recarga.index') }}"><i class="fas fa-mobile"></i> Recargas</a></li>

	@endif

	@if ($currentUser->hasAccess('view-ticket-list'))

		<li><a href="{{ URL::route('ticket.index') }}"><i class="fas fa-file-invoice-dollar"></i> Compra de Saldo</a></li>

	@endif

@endif
