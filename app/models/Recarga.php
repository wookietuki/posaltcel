<?php

/**
 * 
 */
class Recarga extends Eloquent
{
	
	protected $table = "recargas";
	protected $fillable = ['distribuidor_id', 'fecha', 'transnumber', 'phonenumber', 'qty', 'autono', 'responsecode', 'descriptioncode', 'role', 'instr1', 'instr2'];

	private $rules      = [
		"distribuidor_id" => "required",
		"fecha"        		=> "required",
		"transnumber"     => "required",
		"phonenumber"     => 'required',
		"qty"             => 'required',
		"autono"          => 'required',
		"responsecode"    => 'required',
		"descriptioncode" => 'required',
	];

	public function distribuidor()
	{
		return $this->belongsTo('Distribuidor');
	}

  public function validate($data,$id=null)
  {

  	//hace un validador nuevo
			$v = Validator::make($data, $this->rules);
			//checa validación
			if ($v->fails()) {
				//asigna errores y regresa falso
				$this->errors = $v->errors();
				return false;
			}

			//la validación pasa
			return true;

  }

	public function errors()
	{
		return $this->errors;
	}

}