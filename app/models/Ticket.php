<?php

/**
 * 
 */
class Ticket extends Eloquent
{

	protected $table = "tickets";
	protected $fillable = ['fecha', 'distribuidor_id', 'folio', 'cantidad', 'foto_ticket'];

	private $rules = [
		"fecha"    => "required",
		"folio"    => "required",
		"cantidad" => "required",
		//"foto"     => 'required|mimes:jpeg,bmp,png,jpg',
	];



  public function validate($data,$id=null)
  {

  	//hace un validador nuevo
			$v = Validator::make($data, $this->rules);
			//checa validación
			if ($v->fails()) {
				//asigna errores y regresa falso
				$this->errors = $v->errors();
				return false;
			}

			//la validación pasa
			return true;

  }

	public function errors()
	{
		return $this->errors;
	}

}