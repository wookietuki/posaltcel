<?php

/**
 * 
 */
class CantRecarga extends Eloquent
{
	
	protected $table    = 'cant_recargas';
	protected $fillable = ['monto', 'sku', 'visible'];
	
	
	private $rules      = [
		"monto"   => "required|numeric",
		"sku"     => "required|digits:13",
	];

	public function validate($data,$id=null)
  {

  	//hace un validador nuevo
			$v = Validator::make($data, $this->rules);
			//checa validación
			if ($v->fails()) {
				//asigna errores y regresa falso
				$this->errors = $v->errors();
				return false;
			}

			//la validación pasa
			return true;

  }

	public function errors()
	{
		return $this->errors;
	}

}