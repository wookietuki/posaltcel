<?php

/**
 * 
 */
class Peticion extends Eloquent
{
	
	protected $table = "peticiones";
	protected $fillable = ['peticion','respuesta','responsecode','datetime'];

	private $rules = [
		'peticion' => 'required',
		'respuesta' => 'required',
		'responsecode' => 'required',
		'datetime' => 'required'
	];

	public function validate($data,$id=null)
  {

  	//hace un validador nuevo
			$v = Validator::make($data, $this->rules);
			//checa validación
			if ($v->fails()) {
				//asigna errores y regresa falso
				$this->errors = $v->errors();
				return false;
			}

			//la validación pasa
			return true;

  }

	public function errors()
	{
		return $this->errors;
	}
	
}