<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
/**
 * Distribuidores Eloquent
 */
class Distribuidor extends Eloquent
{

	use SoftDeletingTrait;
	
	protected $table    = 'distribuidores';

	protected $fillable = ['user_id', 'distribuidor', 'telefono', 'id_grp', 'id_chain', 'id_merchant', 'id_pos', 'url', 'observaciones'];

	protected $dates    = ['deleted_at'];

	private $rules      = [
		"distribuidor"  => "required",
		"telefono"      => "required",
		"id_grp"        => "required",
		"id_chain"      => 'required',
		"id_merchant"   => 'required',
		"id_pos"        => 'required',
		"url"						=> 'required',
	];

	public function user()
	{
		return $this->belongsTo('User')->withTrashed();
	}

  public function validate($data,$id=null)
  {

  	//hace un validador nuevo
			$v = Validator::make($data, $this->rules);
			//checa validación
			if ($v->fails()) {
				//asigna errores y regresa falso
				$this->errors = $v->errors();
				return false;
			}

			//la validación pasa
			return true;

  }

	public function errors()
	{
		return $this->errors;
	}

}