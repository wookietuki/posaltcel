<?php

/*
INSERT INTO `ra_permissions` ( `name`, `value`, `description`, `created_at`, `updated_at`) VALUES ('Index Recargas', 'view-recarga-list', 'Ver Listado de Recargas', '2018-09-19 08:57:19', '2018-09-19 08:57:19');
INSERT INTO `ra_permissions` ( `name`, `value`, `description`, `created_at`, `updated_at`) VALUES ('Crear Recargas', 'create-recarga', 'Crear Recargas Nuevos', '2018-09-19 08:58:33', '2018-09-19 08:58:33');
INSERT INTO `ra_permissions` ( `name`, `value`, `description`, `created_at`, `updated_at`) VALUES ('Ver Recargas', 'show-recarga-info', 'Ver Información de Recargas', '2018-09-19 08:59:31', '2018-09-19 08:59:31');
INSERT INTO `ra_permissions` ( `name`, `value`, `description`, `created_at`, `updated_at`) VALUES ('Actualizar Recargas', 'update-recarga-info', 'Actualizar Información de Recargas', '2018-09-19 09:00:01', '2018-09-19 09:00:01');
INSERT INTO `ra_permissions` ( `name`, `value`, `description`, `created_at`, `updated_at`) VALUES ('Eliminar Recargas', 'delete-recarga', 'Eliminar Recargas', '2018-09-19 09:00:24', '2018-09-19 09:00:24');

*/

return array(
         'listGroups'                    => 'groups-management',
         'newGroupPost'                  => 'groups-management',
         'newGroup'                      => 'groups-management',
         'deleteGroup'                   => 'groups-management',
         'showGroup'                     => 'groups-management',
         'putGroup'                      => 'groups-management',
         'listUsers'                     => 'view-users-list',
         'deleteUsers'                   => 'delete-user',
         'newUser'                       => 'create-user',
         'newUserPost'                   => 'create-user',
         'showUser'                      => 'update-user-info',
         'putUser'                       => 'update-user-info',
         'putActivateUser'               => 'update-user-info',
         'deleteUserGroup'               => 'user-group-management',
         'addUserGroup'                  => 'user-group-management',
         'listPermissions'               => 'permissions-management',
         'deletePermission'              => 'permissions-management',
         'newPermission'                 => 'permissions-management',
         'newPermissionPost'             => 'permissions-management',
         'showPermission'                => 'permissions-management',
         'putPermission'                 => 'permissions-management',
         'addUserPermission'             => 'permissions-management',
         'addGroupPermission'            => 'permissions-management',
         
         'distribuidor.index'            => 'view-distribuidor-list',
         'distribuidor.create'           => 'create-distribuidor',
         'distribuidor.store'            => 'create-distribuidor',
         'distribuidor.show'             => 'show-distribuidor-info',
         'distribuidor.edit'             => 'update-distribuidor-info',
         'distribuidor.update'           => 'update-distribuidor-info',
         'distribuidor.destroy'          => 'delete-distribuidor',
         
         'catalogo.cant_recarga.index'   => 'view-cant_recarga-list',
         'catalogo.cant_recarga.create'  => 'create-cant_recarga',
         'catalogo.cant_recarga.store'   => 'create-cant_recarga',
         'catalogo.cant_recarga.show'    => 'show-cant_recarga-info',
         'catalogo.cant_recarga.edit'    => 'update-cant_recarga-info',
         'catalogo.cant_recarga.update'  => 'update-cant_recarga-info',
         'catalogo.cant_recarga.destroy' => 'delete-cant_recarga',
         
         'recarga.index'                 => 'view-recarga-list',
         'recarga.create'                => 'create-recarga',
         'recarga.store'                 => 'create-recarga',
         'recarga.show'                  => 'show-recarga-info',
         'recarga.edit'                  => 'update-recarga-info',
         'recarga.update'                => 'update-recarga-info',
         'recarga.destroy'               => 'delete-recarga',
         
         'recurrente.index'              => 'view-recurrente-list',
         'recurrente.create'             => 'create-recurrente',
         'recurrente.store'              => 'create-recurrente',
         'recurrente.show'               => 'show-recurrente-info',
         'recurrente.edit'               => 'update-recurrente-info',
         'recurrente.update'             => 'update-recurrente-info',
         'recurrente.destroy'            => 'delete-recurrente',
         
         'ticket.index'                  => 'view-ticket-list',
         'ticket.create'                 => 'create-ticket',
         'ticket.store'                  => 'create-ticket',
         'ticket.show'                   => 'show-ticket-info',
         'ticket.edit'                   => 'update-ticket-info',
         'ticket.update'                 => 'update-ticket-info',
         'ticket.destroy'                => 'delete-ticket',
         
         'admin.ticket'                  => 'admin-tickets',
         
         'conecta.index'                 => 'view-conecta-list',
         'conecta.create'                => 'create-conecta',
         'conecta.store'                 => 'create-conecta',
         'conecta.show'                  => 'show-conecta-info',
         'conecta.edit'                  => 'update-conecta-info',
         'conecta.update'                => 'update-conecta-info',
         'conecta.destroy'               => 'delete-conecta',

);
