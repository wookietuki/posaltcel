<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets', function($table){
			$table->increments('id')->unsigned();
			$table->integer('distribuidor_id')->unsigned();
			$table->foreign('distribuidor_id')->references('id')->on('distribuidores')->onDelete('cascade');
			$table->date('fecha');
			$table->string('folio', 200);
			$table->double('cantidad', 15, 2);
			$table->string('foto_ticket', 250);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets');
	}

}
