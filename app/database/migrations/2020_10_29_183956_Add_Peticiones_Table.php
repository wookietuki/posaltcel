<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeticionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('peticiones', function($table){
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->text('peticion');
			$table->text('respuesta');
			$table->string('responsecode');
			$table->string('datetime');
			$table->string('telefono', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('peticiones');
	}

}
