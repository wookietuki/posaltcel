<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecargasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recargas', function($table){
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->integer('distribuidor_id')->unsigned();
			$table->foreign('distribuidor_id')->references('id')->on('distribuidores')->onDelete('cascade');
			$table->string('datetime');
			$table->string('transnumber');
			$table->string('phonenumber');
			$table->string('qty');
			$table->string('autono');
			$table->string('responsecode');
			$table->string('descriptioncode');
			$table->string('role');
			$table->string('instr1');
			$table->string('instr2');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recargas');
	}

}
