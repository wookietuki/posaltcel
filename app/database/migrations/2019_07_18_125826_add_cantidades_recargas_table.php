<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCantidadesRecargasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cant_recargas', function($table){
			$table->engine = 'InnoDB';
			$table->increments('id')->unsigned();
			$table->string('monto', 45);
			$table->string('sku', 15);
			$table->boolean('visible');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cant_recargas');
	}

}
