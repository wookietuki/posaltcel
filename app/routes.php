<?php
//prueba
Route::get('/', function(){
	return View::make('hello');
});

Route::get('soporte', function(){
	return View::make('soporte');
});

Route::get('test', function(){
	return View::make('test');
});

Route::post('prueba', array(
	//'before' => 'csrf',
	'as' =>'prueba',
	'uses'=>'MyController@getIndex'
));

/*===========================================
=            Configuración Admin            =
===========================================*/

Config::set('syntara::views.dashboard-index', 'index');

View::composer('dashboard.layouts.dashboard.master', function ($view) {
	$view->with('siteName', 'Recargas Altcel');
	$view->nest('navPages', 'left-nav');
});


/*=====  End of Configuración Admin  ======*/

/*==================================================
=            Contadores Index Principal            =
==================================================*/

View::composer('index', function($view) {
	$view->with('distribuidores', Distribuidor::count())
	     ->with('user', Sentry::getUser())
       ->with('groupDist', Sentry::findGroupByName('Distribuidor'))
       ->with('userDist', Distribuidor::where('user_id',Sentry::getUser()->id)->first())
       ->with('montos',CantRecarga::where('visible',1)->lists('descripcion','id'))
       ->with('totales',TotalRecargaDiaria::all()->take(30));
});

View::composer('sistema.altcel2.index', function($view) {
	$view->with('user', Sentry::getUser())
       ->with('groupDist', Sentry::findGroupByName('Distribuidor'))
       ->with('userDist', Distribuidor::where('user_id',Sentry::getUser()->id)->first());
});

/*=====  End of Contadores Index Principal  ======*/

/*==================================================
=            Contadores Index Altcel            =
==================================================*/

View::composer('sistema.index', function($view) {
	$view->with('distribuidores', Distribuidor::count())
	     ->with('user', Sentry::getUser())
       ->with('groupDist', Sentry::findGroupByName('Distribuidor'))
       ->with('userDist', Distribuidor::where('user_id',Sentry::getUser()->id)->first())
       ->with('montos',CantRecarga::where('visible',1)->lists('descripcion','id'))
       ->with('totales',TotalRecargaDiaria::all()->take(30));
});

/*=====  End of Contadores Index Altcel  ======*/

/*=================================
=            Catálogos            =
=================================*/

View::composer('sistema.cant_recargas.index', function($view){
	$view->with('cant_recargas', CantRecarga::all());
});

Route::group(array('before' => 'basicAuth|hasPermissions', 'prefix' => 'catalogo'), function () {

	Route::resource('cant_recarga', 'CantRecargaController');

});

/*=====  End of Catálogos  ======*/


/*===============================
=            Sistema            =
===============================*/

View::composer('sistema.distribuidores.index', function ($view) {
  $view->with('distribuidores', Distribuidor::all());
});

Route::group(array('before' => 'basicAuth|hasPermissions'), function () {

	Route::resource('distribuidor', 'DistribuidorController');
	Route::resource('recarga', 'RecargaController');
	Route::resource('ticket', 'TicketController');
	Route::resource('conecta', 'ConectaController');
	Route::resource('conecta_cliente', 'ConectaClienteController');
	Route::resource('altcel', 'AltcelController');
	Route::get(
		'ticket-update-altcel2',
		array(
			'as'   => 'ticket.update.altcel2',
			'uses' => 'TicketController@updateAltcel2')
	);
    Route::resource('venta_sim', 'VentaSimController');
    Route::post(
		'venta_sim/actualizar',
		array(
			'as'   => 'venta_sim.actualizar',
			'uses' => 'VentaSimController@actualizar')
	);
});



Route::group(array('before'=>'basicAuth|hasPermissions', 'prefix' => 'recurrente'), function() {
	
	Route::resource('recurrente', 'RecurrenteController');
	Route::resource('empresa', 'EmpresaController');
	Route::resource('telefono', 'TelefonoRecurrenteController');
	Route::resource('correo', 'CorreoRecurrenteController');

});

Route::group(array('before' => 'basicAuth|hasPermissions', 'prefix' => 'altcel2'), function(){
	Route::resource('conecta','Altcel2Controller');

	Route::get(
		'monthly/{msisdn}',
		array(
			'as'   => 'altcel2.monthly',
			'uses' => 'Altcel2Controller@monthly')
	);

	Route::get(
		'save-monthly',
		array(
			'as'   => 'altcel2.save.monthly',
			'uses' => 'Altcel2Controller@saveMonthly')
	);

	Route::get(
		'get-offers-rates-diff/{msisdn}',
		array(
			'as'   => 'altcel2.get.offers.rates.diff',
			'uses' => 'Altcel2Controller@getOffersNRatesDiff')
	);

	Route::get(
		'change.product',
		array(
			'as'   => 'altcel2.change.product',
			'uses' => 'Altcel2Controller@changeProduct')
	);

	Route::get(
		'surplus/{msisdn}',
		array(
			'as'   => 'altcel2.surplus',
			'uses' => 'Altcel2Controller@surplus')
	);

	Route::get(
		'purchase-product',
		array(
			'as'   => 'altcel2.purchase',
			'uses' => 'Altcel2Controller@purchaseProduct')
	);

	Route::get(
		'verify-exists-msisdn',
		array(
			'as'   => 'altcel2.verify.exists.msisdn',
			'uses' => 'Altcel2Controller@verifyExistsMSISDN')
	);

	Route::get(
		'unbarring',
		array(
			'as'   => 'altcel2.unbarring',
			'uses' => 'Altcel2Controller@unbarring')
	);
});

/*=====  End of Sistema  ======*/

/*==================================
=  Recuperación de contraseña      =
==================================*/

Route::get(
	'recover_password/{id}/{resetCode}',
	array(
		'as'   => 'recover.newPassword',
		'uses' => 'RecoverController@newPassword')
);


Route::post(
	'recover_password/{id}/{resetCode}',
	array(
	'as'   => 'recover.newPasswordPost',
	'uses' => 'RecoverController@newPasswordPost')
);

Route::resource('recover', 'RecoverController');

/*=====  End of Recuperación de contraseña  ======*/
