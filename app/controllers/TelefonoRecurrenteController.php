<?php

class TelefonoRecurrenteController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout             = View::make('sistema.telefonos.create');
		$this->layout->title      = 'Línea Nueva';
		$this->layout->empresa_id = Input::get('empresa_id');
		$this->layout->montos     = CantRecarga::where('visible',1)->lists('monto','id');
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Línea Nueva',
		    'link'  => 'recurrente/telefono/create',
		    'icon'  => 'fas fa-phone'
		  ),
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$new = Input::all();
		$telefono = new Telefono();

		if (!$telefono->validate($new))
		{
	    $errors = $telefono->errors();
	    return Redirect::route('recurrente.telefono.create')->withErrors($errors)->withInput();
		}


		if(Input::has('activo')) {
			Input::merge(['activo'=>1]);
		} else {
			Input::merge(['activo'=>0]);
		}

		$alta = Carbon::parse(Input::get('alta'));

		$ultima = $alta->addDay(Input::get('recurrencia'));

		Input::merge(['ultima'=>$ultima->format('Y-m-d')]);

		//return dump(Input::all());

		$telefono->create(Input::all());
		return Redirect::route('recurrente.empresa.show',Input::get('empresa_id'))->with('success','¡Se ha creado con éxito la Línea!');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout             = View::make('sistema.telefonos.create');
		$this->layout->title      = 'Editar Línea';
		$this->layout->empresa_id = Input::get('empresa_id');
		$this->layout->montos     = CantRecarga::where('visible',1)->lists('monto','id');
		$this->layout->telefono   = Telefono::find($id);
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Editar Línea',
		    'link'  => 'recurrente/telefono/'.$id.'/edit',
		    'icon'  => 'fas fa-phone'
		  ),
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$telefono = Telefono::find($id);

		if(Input::has('activo')) {
			Input::merge(['activo'=>1]);
		} else {
			Input::merge(['activo'=>0]);
		}
		
		$data = Input::all();

		if (!$telefono->validate($data,$id))
		{
	    $errors = $telefono->errors();
	    return Redirect::route('recurrente.telefono.edit',$id)->withErrors($errors)->withInput();
		}

		//return dump(Input::all());

		$telefono->update(Input::all());
		return Redirect::route('recurrente.empresa.show',Input::get('empresa_id'))->with('success','¡Se ha actualizado con éxito la Línea!');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Telefono::destroy($id);
		return Redirect::back()->with('info','Se ha eliminado con éxito el número de teléfono');
	}


}
