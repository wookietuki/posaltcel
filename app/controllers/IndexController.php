<?php

class IndexController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

    $this->layout           = View::make('sistema.index_1');
		$this->layout->title    = 'Inicio';
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		);

  }

  public function recargaAltcel()
  {
    $this->layout           = View::make('index');
		$this->layout->title    = 'Inicio';
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		);
  }

  public function recargaConecta()
  {
    $this->layout            = View  ::make('sistema.conecta.index');
		$this->layout->title     = 'Altcel Conecta';
		$this->layout->user      = Sentry::getUser();
		$this->layout->groupDist = Sentry::findGroupByName('Distribuidor');
		$this->layout->userDist  = Distribuidor::where('user_id',Sentry::getUser()->id)->first();
		$this->layout->montos    = CantRecarga::where('visible',1)->lists('monto','id');
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Altcel Conecta',
		    'link'  => 'conecta',
		    'icon'  => 'fas fa-plug'
		  ),
		);
  }


}