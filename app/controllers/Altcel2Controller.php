<?php

class Altcel2Controller extends \BaseController {

    public function index(){
        $this->layout        = View::make('sistema.altcel2.index');
		    $this->layout->title = 'Clientes Conecta';
        // add breadcrumb to current page
        $this->layout->breadcrumb = array(
          array(
            'title' => 'Inicio',
            'link'  => '/',
            'icon'  => 'fas fa-home'
          ),
          array(
            'title' => 'Recargas Conecta',
            'link'  => 'altcel2/conecta',
            'icon'  => 'fas fa-cash-register'
          ),
        );
    }

    public function monthly($msisdn){
      $url = "http://altcel2.com/my-conecta-actions?msisdn=".$msisdn."&action=monthly";
      $http = new \GuzzleHttp\Client();

      $response = $http->request('GET',$url,[
        'debug' => FALSE,
        'headers' => [
              'Content-Type' => 'application/x-www-form-urlencoded'
          ]
      ]);

      $jsonString = $response->getBody();

      $respuesta = json_decode($jsonString,true);
      return $respuesta;
    }

    public function saveMonthly(){
      $request = Input::all();
      $amount = $request['monto'];
      $dealer_id = $request['dealer_id'];

      $dataDealer = Distribuidor::where('user_id',$dealer_id)->first();
      $saldo_conecta = $dataDealer->saldo_conecta;
      $tblDealerID = $dataDealer->id;

      if($saldo_conecta >= $amount){
        // return Input::all();
        $url = "http://187.217.216.244/save-manual-pay-pos";
        $http = new \GuzzleHttp\Client();

        $response = $http->request('POST',$url,[
          'form_params' => Input::all(),
          'headers' => [
              'Content-Type' => 'application/x-www-form-urlencoded'
          ]
        ]);

        $jsonString = $response->getBody();

        $respuesta = json_decode($jsonString,true);

        $recarga = new Recarga();
        $TransNumber = rand ( 1 , 99999 );
        $Instr1 = '';
        $Instr2 = '';

        if($respuesta == 1){
          $saldo_conecta-=$amount;
          Distribuidor::where('user_id',$dealer_id)->update(['saldo_conecta'=>$saldo_conecta]);

          //aqui
          // return $respuesta;

          Input::merge([
              'distribuidor_id' => $tblDealerID, 
              'fecha' => date('Y-m-d H:i:s'), 
              'transnumber' => $TransNumber, 
              'phonenumber' => Input::get('msisdn'), 
              'qty' => $amount, 
              'autono' => $TransNumber, // OrderID 
              'responsecode' => 0,  // código de respuesta
              'descriptioncode' => 'Transacción exitosa',
              'role' => 'success',
              'instr1' => $Instr1,
              'instr2' => $Instr2,
          ]);  
    
          $id = $recarga->create(Input::all());
          //aqui
          
          return $jsonString;
        }else{

          //aqui
          // return $respuesta;

          Input::merge([
              'distribuidor_id' => $tblDealerID, 
              'fecha' => date('Y-m-d H:i:s'), 
              'transnumber' => $TransNumber, 
              'phonenumber' => Input::get('msisdn'), 
              'qty' => $amount, 
              'autono' => $TransNumber, // OrderID 
              'responsecode' => 500,  // código de respuesta
              'descriptioncode' => 'Transacción sin completar',
              'role' => 'danger',
              'instr1' => $Instr1,
              'instr2' => $Instr2,
          ]);  
    
          $id = $recarga->create(Input::all());
          //aqui
          return $jsonString;
        }
      }else{
        return 2;
      }
    }

    public function getOffersNRatesDiff($msisdn){
      // return $msisdn;
      $url = "http://187.217.216.244/get-offers-rates-diff-pos?msisdn=".$msisdn;
        $http = new \GuzzleHttp\Client();

        $response = $http->request('GET',$url,[
          'debug' => FALSE,
          'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ]);

        $jsonString = $response->getBody();

        $respuesta = json_decode($jsonString,true);
        return $jsonString;
    }

    public function changeProduct(){
      $request = Input::all();
      $amount = $request['amount'];
      $dealer_id = $request['dealer_id'];

      $dataDealer = Distribuidor::where('user_id',$dealer_id)->first();
      $saldo_conecta = $dataDealer->saldo_conecta;
      $tblDealerID = $dataDealer->id;

      if($saldo_conecta >= $amount){
        $url = "http://187.217.216.244/change-product-pos";
        $http = new \GuzzleHttp\Client();

        $response = $http->request('POST',$url,[
            'form_params' => Input::all(),
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ]);

        $jsonString = $response->getBody();

        $respuesta = json_decode($jsonString,true);
        $http_code = $respuesta['http_code'];

        $recarga = new Recarga();
        $TransNumber = rand ( 1 , 99999 );
        $Instr1 = '';
        $Instr2 = '';

        if($http_code == 1 || $http_code == 2){
          // return $respuesta;
          $saldo_conecta-=$amount;
          Distribuidor::where('user_id',$dealer_id)->update(['saldo_conecta'=>$saldo_conecta]);

          //aqui
          // return $respuesta;

          Input::merge([
              'distribuidor_id' => $tblDealerID, 
              'fecha' => date('Y-m-d H:i:s'), 
              'transnumber' => $TransNumber, 
              'phonenumber' => Input::get('msisdn'), 
              'qty' => $amount, 
              'autono' => $respuesta['order'], // OrderID 
              'responsecode' => 0,  // código de respuesta
              'descriptioncode' => 'Transacción exitosa',
              'role' => 'success',
              'instr1' => $Instr1,
              'instr2' => $Instr2,
          ]);  
    
          $id = $recarga->create(Input::all());
          //aqui

          return $respuesta;
        }else{

          Input::merge([
              'distribuidor_id' => $tblDealerID, 
              'fecha' => date('Y-m-d H:i:s'), 
              'transnumber' => $TransNumber, 
              'phonenumber' => Input::get('msisdn'), 
              'qty' => $amount, 
              'autono' => $respuesta['order'], // OrderID 
              'responsecode' => 0,  // código de respuesta
              'descriptioncode' => $respuesta['message'],
              'role' => 'danger',
              'instr1' => $Instr1,
              'instr2' => $Instr2,
          ]);  
    
          $id = $recarga->create(Input::all());
          return $respuesta;
        }
        
      }else{
        $respuesta = array('http_code'=>3,'message'=>'Al parecer no cuestas con saldo suficiente, actualiza la ventana.');
        return $respuesta;
      }
    }

    public function surplus($msisdn){

        $url = "http://187.217.216.244/surplusRatesDealerAPI/".$msisdn;
        $http = new \GuzzleHttp\Client();

        $response = $http->request('GET',$url,[
            'debug' => FALSE,
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);

        $jsonString = $response->getBody();

        $respuesta = json_decode($jsonString,true);
        // $dataMSISDN = $respuesta['dataMSISDN'];
        // $packsSurplus = $respuesta['packsSurplus'];
        // return $dataMSISDN;
        return $respuesta;
    }

    public function purchaseProduct(){
      $request = Input::all();
      $price = $request['price'];
      $dealer_id = $request['dealer_id'];

      $dataDealer = Distribuidor::where('user_id',$dealer_id)->first();
      $saldo_conecta = $dataDealer->saldo_conecta;
      $tblDealerID = $dataDealer->id;

      if($saldo_conecta >= $price){

        $url = "http://187.217.216.244/purchase-pos";
        $http = new \GuzzleHttp\Client();

        $response = $http->request('POST',$url,[
            'form_params' => Input::all(),
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
        ]);

        $jsonString = $response->getBody();

        $respuesta = json_decode($jsonString,true);
        $http_code = $respuesta['http_code'];

        $recarga = new Recarga();
        $TransNumber = rand ( 1 , 99999 );
        $Instr1 = '';
        $Instr2 = '';

        if($http_code == 1){
          $saldo_conecta-=$price;
          Distribuidor::where('user_id',$dealer_id)->update(['saldo_conecta'=>$saldo_conecta]);
          //aqui
          // return $respuesta;

          Input::merge([
              'distribuidor_id' => $tblDealerID, 
              'fecha' => date('Y-m-d H:i:s'), 
              'transnumber' => $TransNumber, 
              'phonenumber' => Input::get('msisdn'), 
              'qty' => $price, 
              'autono' => $respuesta['order'], // OrderID 
              'responsecode' => 0,  // código de respuesta
              'descriptioncode' => 'Transacción exitosa',
              'role' => 'success',
              'instr1' => $Instr1,
              'instr2' => $Instr2,
          ]);  
    
          $id = $recarga->create(Input::all());
          //aqui
          return $respuesta;
        }else{

          Input::merge([
              'distribuidor_id' => $tblDealerID, 
              'fecha' => date('Y-m-d H:i:s'), 
              'transnumber' => $TransNumber, 
              'phonenumber' => Input::get('msisdn'), 
              'qty' => $price, 
              'autono' => $respuesta['order'], // OrderID 
              'responsecode' => 500,  // código de respuesta
              'descriptioncode' => $response['message'],
              'role' => 'danger',
              'instr1' => $Instr1,
              'instr2' => $Instr2,
          ]);  
    
          $id = $recarga->create(Input::all());
          return $respuesta;
        }
      }else{
        // return 'BAD';
        $respuesta = array('http_code'=>2,'message'=>'Al parecer no cuestas con saldo suficiente, actualiza la ventana.');
        return $respuesta;
      }
        
    }

    public function verifyExistsMSISDN(){
      $msisdn = Input::get('msisdn');

      $url = 'http://187.217.216.244/verify-exists-msisdn/'.$msisdn;
      $http = new \GuzzleHttp\Client();

      $response = $http->request('GET',$url,[
          'headers' => [
              'Content-Type' => 'application/json'
          ]
      ]);

      $jsonString = $response->getBody();

      $respuesta = json_decode($jsonString,true);
      return $respuesta;
      
    }

    public function unbarring(){
      $payID = Input::get('payID');
      $url = 'http://187.217.216.244/unbarring-pos?payID='.$payID;
      $http = new \GuzzleHttp\Client();

      $response = $http->request('GET',$url,[
          'headers' => [
              'Content-Type' => 'application/x-www-form-urlencoded'
          ]
      ]);

      $jsonString = $response->getBody();

      $respuesta = json_decode($jsonString,true);
      return $jsonString;
    }
}