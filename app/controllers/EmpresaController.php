<?php

class EmpresaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout           = View::make('sistema.empresas.index');
		$this->layout->title    = 'Empresas';
		$this->layout->empresas = Empresa::all();
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Empresas',
		    'link'  => 'empresa',
		    'icon'  => 'fas fa-building'
		  ),
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout        = View::make('sistema.empresas.create');
		$this->layout->title = 'Empresa Nueva';
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Empresa Nueva',
		    'link'  => 'recurrente/empresa/create',
		    'icon'  => 'fas fa-building'
		  ),
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$new = Input::all();
		$empresa = new Empresa();

		if (!$empresa->validate($new))
		{
	    $errors = $empresa->errors();
	    return Redirect::route('recurrente.empresa.create')->withErrors($errors)->withInput();
		}

		$empresa->create(Input::all());
		return Redirect::route('recurrente.empresa.index')->with('success','¡Se ha creado con éxito la Empresa!');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->layout            = View::make('sistema.empresas.show');
		$this->layout->empresa   = Empresa::find($id);
		$this->layout->title     = $this->layout->empresa->nombre;
		$this->layout->telefonos = Telefono::where('empresa_id',$id)->get();

		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Empresas',
		    'link'  => 'recurrente/empresa',
		    'icon'  => 'fas fa-building'
		  ),
		  array(
		    'title' => $this->layout->empresa->nombre,
		    'link'  => '#',
		    'icon'  => 'fas fa-building'
		  ),
		);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout          = View::make('sistema.empresas.create');
		$this->layout->title   = 'Edición Empresa';
		$this->layout->empresa = Empresa::find($id);
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Edición Empresa',
		    'link'  => 'recurrente/empresa/'.$id.'/edit',
		    'icon'  => 'fas fa-building'
		  ),
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$empresa = Empresa::find($id);
		$edit         = Input::all();

		if (!$empresa->validate($edit))
		{
	    $errors = $empresa->errors();
	    return Redirect::back()->withErrors($errors)->withInput();
		}

		$empresa->update(Input::all());
		return Redirect::route('recurrente.empresa.index')->with('success','¡Se ha actualizado con éxito la Empresa!');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Empresa::destroy($id);
		return Redirect::back()->with('info','Se ha eliminado la Empresa');
	}


}
