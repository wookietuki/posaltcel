<?php

class ConectaClienteController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout        = View::make('sistema.conecta_clientes.index');
		$this->layout->title = 'Clientes Conecta';
		$this->layout->conecta_clientes = ConectaCliente::all();
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Clientes Conecta',
		    'link'  => 'conecta_cliente',
		    'icon'  => 'fas fa-user'
		  ),
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout        = View::make('sistema.conecta_clientes.create');
		$this->layout->title = 'Cliente Conecta Nuevo';
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Cliente Conecta Nuevo',
		    'link'  => 'conecta_cliente/create',
		    'icon'  => 'fas fa-user'
		  ),
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$api_key = \Conekta\Conekta::setApiKey("key_vcGgb4BdxDtnMe47a8VGxg"); //Cconekta Prueba
		//$api_key = \Conekta\Conekta::setApiKey("key_7VkURZx6xRvP8rpscj3yYg"); //Cconekta Produccion
		$api_version = \Conekta\Conekta::setApiVersion("2.0.0");
		
		//return dump(Input::all());
		try{
			$customer = \Conekta\Customer::create(
				array(
						'name'  => Input::get('nombre_cliente')." ".Input::get('apellido_cliente'),
						'email' => Input::get('email'),
						'phone' => "+521".Input::get('tel_altan'),
						'payment_sources' => array(
									array(
									'type' => "oxxo_recurrent",
									)
							)
				)
			);
			$conekta_id            = (json_encode($customer->id));
		  $conekta_reference     = (json_encode($customer->payment_sources[0]->reference));
		  $conekta_reference_url = (json_encode($customer->payment_sources[0]->barcode_url));
			//dump(json_encode($customer->payment_sources[0]));
		} catch (\Conekta\ProcessingError $error){
			echo $error->getMessage();
		} catch (\Conekta\ParameterValidationError $error){
			echo $error->getMessage();
		} catch (\Conekta\Handler $error){
			echo $error->getMessage();
		}

		$data = [
			$customer
		];

		$new = Input::all();
		$conectaCliente = new ConectaCliente();
		
		Input::merge([
			'conekta_id'            => json_decode($conekta_id),
			'conekta_reference'     => json_decode($conekta_reference),
			'conekta_reference_url' => json_decode($conekta_reference_url)
		]);

		if (!$conectaCliente->validate(Input::all()))
		{
	    $errors = $conectaCliente->errors();
	    return Redirect::to('conecta_cliente/create')->withErrors($errors)->withInput();
		}

		ConectaCliente::create(Input::all());
		return Redirect::to('conecta_cliente')->with('info','El cliente se ha guardado con exito!');
	

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout        = View::make('sistema.conecta_clientes.create');
		$this->layout->title = 'Cliente Conecta Edición';
		$this->layout->conecta_cliente = ConectaCliente::find($id);
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Cliente Conecta Edición',
		    'link'  => 'conecta_cliente/create',
		    'icon'  => 'fas fa-user'
		  ),
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$conectaCliente = ConectaCliente::find($id);

		Input::merge([
			'conekta_id'            => $conectaCliente->conekta_id,
			'conekta_reference'     => $conectaCliente->conekta_reference,
			'conekta_reference_url' => $conectaCliente->conekta_reference_url
		]);

		if (!$conectaCliente->validate(Input::all()))
		{
	    $errors = $conectaCliente->errors();
	    return Redirect::to('conecta_cliente/create')->withErrors($errors)->withInput();
		}

		if ($conectaCliente->update(Input::all())) {
			return Redirect::to('conecta_cliente')->with('success','El cliente se ha guardado con exito');
		}

		return Redirect::to('conecta_cliente/'.$id.'/edit')->with('error','Ha ocurrido un error. Intente nuevamente.')->withInput();

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		ConectaCliente::destroy($id);
		return Redirect::back()->with('info','Se ha eliminado al Cliente');

	}


}
