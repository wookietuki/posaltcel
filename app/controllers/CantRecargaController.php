<?php

class CantRecargaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout        = View::make('sistema.cant_recargas.index');
		$this->layout->title = 'Tipos de Recarga';
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Tipos de Recarga',
		    'link'  => 'catalogo/cant_recarga',
		    'icon'  => 'fas fa-file-invoice-dollar'
		  ),
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout        = View::make('sistema.cant_recargas.create');
		$this->layout->title = 'Nuevo Tipo de Recarga';
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Nuevo Tipo de Recarga',
		    'link'  => 'catalogo/cant_recarga/create',
		    'icon'  => 'fas fa-file-invoice-dollar'
		  ),
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$new = Input::all();
		$cant_recarga = new CantRecarga();

		if (!$cant_recarga->validate($new))
		{
	    $errors = $cant_recarga->errors();
	    return Redirect::route('catalogo.cant_recarga.create')->withErrors($errors)->withInput();
		}


		if(Input::has('visible')) {
			Input::merge(['visible'=>1]);
		} else {
			Input::merge(['visible'=>0]);
		}

		//return dump(Input::all());

		$cant_recarga->create(Input::all());
		return Redirect::route('catalogo.cant_recarga.index')->with('success','¡Se ha creado con éxito el Tipo de Recarga!');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout               = View::make('sistema.cant_recargas.create');
		$this->layout->title        = 'Edición Tipo de Recarga';
		$this->layout->cant_recarga = CantRecarga::find($id);
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Edición Tipo de Recarga',
		    'link'  => 'catalogo/cant_recarga/'.$id.'/edit',
		    'icon'  => 'fas fa-file-invoice-dollar'
		  ),
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$cant_recarga = CantRecarga::find($id);
		$edit         = Input::all();

		if (!$cant_recarga->validate($edit))
		{
	    $errors = $cant_recarga->errors();
	    return Redirect::back()->withErrors($errors)->withInput();
		}

		if(Input::has('visible')) {
			Input::merge(['visible'=>1]);
		} else {
			Input::merge(['visible'=>0]);
		}

		//return dump(Input::all());

		$cant_recarga->update(Input::all());
		return Redirect::route('catalogo.cant_recarga.index')->with('success','¡Se ha actualizado con éxito el Tipo de Recarga!');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		CantRecarga::destroy($id);
		return Redirect::back()->with('info','Se ha eliminado el Tipo de Recarga');
	}


}
