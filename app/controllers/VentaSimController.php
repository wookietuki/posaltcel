<?php

class VentaSimController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout          = View::make('sistema.ventaSim.index');
		$this->layout->title   = 'Ventas SIM';
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Ventas SIM',
		    'link'  => 'venta_sim',
		    'icon'  => 'fas fa-sim-card'
		  ),
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout          = View::make('sistema.ventaSim.portabilidad');
		$this->layout->title   = 'Ventas SIM Portabilidad';
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Ventas SIM Portabilidad',
		    'link'  => 'venta_sim',
		    'icon'  => 'fas fa-sim-card'
		  ),
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $new = Input::all();
		$portabilidad = new Portabilidade();
        //return dump(Sentry::getUser());
		$distribuidor = "Distribuidor: ".Sentry::getUser()->first_name." ".Sentry::getUser()->last_name;
		Input::merge(['status'=>'Solicitud','promotor'=>$distribuidor]);
		$sim = Sim::where('iccid',Input::get('iccid'))->first();
		if ($sim == null) {
			return Redirect::back()->with('error','El numero de SIM NO existe, intenta de nuevo con otro SIM.')->withInput();
		}
		
		$sim = Sim::where('iccid',Input::get('iccid'))->where('usado',1)->count();
		if ($sim > 0) {
			return Redirect::back()->with('error','El numero de SIM ya ha sido utilizado, intenta de nuevo con otro SIM.')->withInput();
		}
		//return Input::all();
		
		//return ($sim);
		
		$portabilidad->create(Input::all());

		//return $portabilidad;

		$data = Input::all();
		/* Envio de Mail */
		Mail::send('emails.portabilidad', $data, function($message)
		{
	    $message->to('alejandro_rueda@altcel.com', 'Alejandro Rueda')->subject('Solicitud de Portabilidad');
	    $message->to('joel_maza@altcel.com', 'Joel Maza')->subject('Solicitud de Portabilidad');
	    $message->to('carlos_vazquez@altcel.com', 'Carlos Velazquez')->subject('Solicitud de Portabilidad');
	    $message->to('jonathan_gutierrez@altcel.com', 'Jonathan Gutierrez')->subject('Solicitud de Portabilidad');
	    $message->to('leopoldo_martinez@altcel.com', 'Leopoldo Martinez')->subject('Solicitud de Portabilidad');
	    $message->to('laura_coutino@altcel.com', 'Laura Coutiño Gonzalez')->subject('Solicitud de Portabilidad');
		});

		if (Mail::failures()) {
			return Redirect::back()->with('error','Ha ocurrido un error, intenta de nuevo más tarde!')->withInput();
		}else{
			$sim = Sim::where('iccid',Input::get('iccid'))->first();
			$sim->usado = 1;
			$sim->save();
			return Redirect::back()->with('success','Hemos recibido con éxito tus datos. Pronto nos pondrémos en contacto contigo!');
		}
		return dump(Input::all());
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout          = View::make('sistema.ventaSim.nueva');
		$this->layout->title   = 'Ventas SIM Linea Nueva';
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Ventas SIM Linea Nueva',
		    'link'  => 'venta_sim',
		    'icon'  => 'fas fa-sim-card'
		  ),
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$url = "http://187.217.216.242/webhook/solicitudlineanueva";
        $client = new \GuzzleHttp\Client();

        $res = $client->request('POST', $url, [
        'debug' => FALSE,
        'form_params' => Input::all(),
        'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]
        ]);
        $jsonString = $res->getBody();

        $respuesta = json_decode($jsonString, true);
        dump($respuesta);

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function actualizar()
	{
        $sim = Sim::where('iccid',Input::get('iccid'))->first();
		if ($sim == null) {
			return Redirect::back()->with('error','El numero de SIM NO existe, intenta de nuevo con otro SIM.')->withInput();
		}
		
		$sim = Sim::where('iccid',Input::get('iccid'))->where('usado',1)->count();
		if ($sim > 0) {
			return Redirect::back()->with('error','El numero de SIM ya ha sido utilizado, intenta de nuevo con otro SIM.')->withInput();
		}

        Input::merge(['solicito' => 'Distribuidor: '.Sentry::getUser()->first_name.' '.Sentry::getUser()->last_name, 'status' => 'Solicitud','price_plan_nuevo_id'=>8,'tarifa_nuevo_id'=>40]);
		$url = "http://187.217.216.242/webhook/solicitudlineanueva";
        $client = new \GuzzleHttp\Client();

        $res = $client->request('POST', $url, [
        'debug' => FALSE,
        'form_params' => Input::all(),
        'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]
        ]);
        $jsonString = $res->getBody();

        $respuesta = json_decode($jsonString, true);
        
        $sim = Sim::where('iccid',Input::get('iccid'))->first();
        $sim->usado = 1;
        $sim->save();
        return Redirect::back()->with('success',$respuesta);

	}


}
