<?php

class DistribuidorController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout        = View::make('sistema.distribuidores.index');
		$this->layout->title = 'Distribuidores';
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Distribuidores',
		    'link'  => 'distribuidores',
		    'icon'  => 'fas fa-cash-register'
		  ),
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout        = View::make('sistema.distribuidores.create');
		$this->layout->title = 'Distribuidor Nuevo';
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Distribuidor Nuevo',
		    'link'  => 'distribuidor/create',
		    'icon'  => 'fas fa-cash-register'
		  ),
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$new = Input::all();
		$distribuidor = new Distribuidor();

		if (!$distribuidor->validate($new))
		{
	    $errors = $distribuidor->errors();
	    return Redirect::to('distribuidor/create')->withErrors($errors)->withInput();
		}

		try
		{
	    // Create the user
	    $user = Sentry::createUser(array(
				'email'      => Input::get('email'),
				'password'   => 'secreto',
				'activated'  => true,
				'username'   => Input::get('username'),
				'first_name' => Input::get('first_name'),
				'last_name'  => Input::get('last_name'),
	    ));

	    // Find the group using the group id
	    $distribuidorGroup = Sentry::findGroupByName('Distribuidor');

	    // Assign the group to the user
	    $user->addGroup($distribuidorGroup);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		  //echo 'Login field is required.';
		  return Redirect::to('distribuidor/create')->with('error','Login.')->withInput();
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		  //echo 'Password field is required.';
		  return Redirect::to('distribuidor/create')->with('error','Password.')->withInput();
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		  //echo 'User with this login already exists.';
		  return Redirect::to('distribuidor/create')->with('error','Ya Existe el usuario: '.Input::get('username'))->withInput();
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		  //echo 'Group was not found.';
		  return Redirect::to('distribuidor/create')->with('error','Grupo.')->withInput();
		}
		catch (Exception $e)
		{
			if($e->errorInfo[0] === '23000'){
				return Redirect::to('distribuidor/create')->with('error','Ya se esta usando el correo: '.Input::get('email'))->withInput();
			} else{
				return Redirect::to('distribuidor/create')->with('error','Ha ocurrido un error, Intente de nuevo.')->withInput();
			}
		}

		Input::merge(['user_id'=>$user->id]);
		Distribuidor::create(Input::all());
		return Redirect::to('distribuidor')->with('info','El distribuidor se ha guardado con exito <br> Usuario: '.$user->username.' <br>Contraseña: secreto <br>Por favor pida al Distribuidor que cambie su contraseña inmediatamente.');
	
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->layout               = View::make('sistema.distribuidores.show');
		$this->layout->distribuidor = Distribuidor::find($id);
		$this->layout->title        = 'Distribuidor: '.$this->layout->distribuidor->user->first_name.' '.$this->layout->distribuidor->user->last_name;
		$this->layout->tickets      = Ticket::where('distribuidor_id',$this->layout->distribuidor->id)->get();
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Distribuidor: '.$this->layout->distribuidor->user->first_name.' '.$this->layout->distribuidor->user->last_name,
		    'link'  => '#',
		    'icon'  => 'fas fa-cash-register'
		  ),
		);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout               = View::make('sistema.distribuidores.create');
		$this->layout->title        = 'Distribuidor';
		$this->layout->distribuidor = Distribuidor::find($id);
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Distribuidor',
		    'link'  => 'distribuidor/'.$id.'/edit',
		    'icon'  => 'fas fa-cash-register'
		  ),
		);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// create a new model instance
		$distribuidor = Distribuidor::find($id);

		// attempt validation

		try
		{
	    // Find the user using the user id
	    $user = Sentry::findUserById($distribuidor->user->id);
	    //return dump($user);
	    // Update the user details
	    $user->first_name = Input::get('first_name');
	    $user->last_name = Input::get('last_name');

	    if ($user->save()) {
				$edit = Input::all();

				if (!$distribuidor->validate($edit,$id))
				{
			    $errors = $distribuidor->errors();
			    return Redirect::to('distribuidor/'.$id.'/edit')->withErrors($errors)->withInput();
				}


				if ($distribuidor->update(Input::all())) {
					return Redirect::to('distribuidor')->with('success','El distribuidor se ha guardado con exito');
				}

				return Redirect::to('distribuidor/'.$id.'/edit')->with('error','Ha ocurrido un error. Intente nuevamente.')->withInput();
	    }
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		  //echo 'Login field is required.';
		  return Redirect::to('distribuidor/'.$id.'/edit')->with('error','Login.')->withInput();
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		  //echo 'Password field is required.';
		  return Redirect::to('distribuidor/'.$id.'/edit')->with('error','Password.')->withInput();
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		  //echo 'User with this login already exists.';
		  return Redirect::to('distribuidor/'.$id.'/edit')->with('error','Ya Existe el usuario: '.Input::get('username'))->withInput();
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		  //echo 'Group was not found.';
		  return Redirect::to('distribuidor/'.$id.'/edit')->with('error','Grupo.')->withInput();
		}
		catch (Exception $e)
		{
			if($e->errorInfo[0] === '23000'){
				return Redirect::to('distribuidor/'.$id.'/edit')->with('error','Ya se esta usando el correo: '.Input::get('email'))->withInput();
			} else{
				return Redirect::to('distribuidor/'.$id.'/edit')->with('error','Ha ocurrido un error, Intente de nuevo.')->withInput();
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Distribuidor::destroy($id);
		return Redirect::back()->with('info','Se ha eliminado al Distribuidor');
	}


}
