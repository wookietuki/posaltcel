<?php

class ConectaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout            = View  ::make('sistema.conecta.index');
		$this->layout->title     = 'Altcel Conecta';
		$this->layout->user      = Sentry::getUser();
		$this->layout->groupDist = Sentry::findGroupByName('Distribuidor');
		$this->layout->userDist  = Distribuidor::where('user_id',Sentry::getUser()->id)->first();
		$this->layout->montos    = CantRecarga::where('visible',1)->lists('monto','id');
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Altcel Conecta',
		    'link'  => 'conecta',
		    'icon'  => 'fas fa-plug'
		  ),
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//return Input::all();

		$conectaCliente = ConectaCliente::where('tel_altan',Input::get('telefono'))->first();

		if ($conectaCliente == null) {
			return Redirect::back()->with('info','Este Teléfono no tiene asignado ningun paquete Conecta.')->withInput();
		}

		$user = Sentry::getUser();
		$groupDist = Sentry::findGroupByName('Distribuidor');
		$userDist = Distribuidor::where('user_id',Sentry::getUser()->id)->first();
		$saldoConecta = $userDist->saldo;

		$urlsaldo = $userDist->url.'ServicePX.asmx/SaldoDisponible?lGrupo='.$userDist->id_grp.'&lCadena='.$userDist->id_chain.'&lTienda='.$userDist->id_merchant;


    $client = new \GuzzleHttp\Client(); 
    try {
      $response = $client->request(
        'GET', 
        (($urlsaldo)), 
        [ 
          'headers' => [
            'Content-Type' => 'text/xml; charset=UTF8',
            'Accept' => 'application/xml'
          ], 
          'timeout' => 50
        ])->getBody()->getContents(); 
    } catch (Exception $e) {
      echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }


    function get_string_between($string, $start, $end){
      $string = ' ' . $string;
      $ini = strpos($string, $start);
      if ($ini == 0) return '';
      $ini += strlen($start);
      $len = strpos($string, $end, $ini) - $ini;
      return substr($string, $ini, $len);
    }
    if(isset($response)){
      $saldo = get_string_between(htmlspecialchars_decode($response), '<double xmlns="http://www.pagoexpress.com.mx/ServicePX">', '</double>');
    } else {
      $saldo = '0';
    }

    //$saldo = 0;
		//$saldoConecta = 0;
    $monto = CantRecarga::find(Input::get('monto'));

    if ($saldo <= 0 || $monto->monto > $saldo) {
    	return Redirect::back()->with('error','El Saldo Altcel no es suficiente')->withInput();
    }

		if ($saldoConecta <= 0 || $monto->monto > $saldoConecta) {
    	return Redirect::back()->with('error','El Saldo Conecta no es suficiente')->withInput();
    }

		/*
    dump($saldo);
    dump(Input::all());
    dump($monto->monto);
		dump($userDist->saldo);
		dump($conectaCliente);
		*/

		$monto = CantRecarga::find(Input::get('monto'));
    $role = "danger";
		$tel = $conectaCliente->tel_altcel;
		$sku = $monto->sku;
		$diahora = date('d/m/Y H:i:s');
		$TransNumber = rand ( 1 , 99999 );
		$DescripcionCode = "";
		$DateTime = $diahora;
		$AutoNo = 0;
		$Instr1 = "";
		$Instr2 = "";

		$string = "<ReloadRequest><ID_GRP>".$userDist->id_grp."</ID_GRP><ID_CHAIN>".$userDist->id_chain."</ID_CHAIN><ID_MERCHANT>".$userDist->id_merchant."</ID_MERCHANT><ID_POS>".$userDist->id_pos."</ID_POS><DateTime>".$diahora."</DateTime><SKU>".$sku."</SKU><PhoneNumber>".$tel."</PhoneNumber><TransNumber>".$TransNumber."</TransNumber><ID_COUNTRY>0</ID_COUNTRY><TC>0</TC></ReloadRequest>";
  	
  	$url = $userDist->url.'ServicePX.asmx/getReloadClass?sXML='.($string);

  	$client = new \GuzzleHttp\Client(); 
	  try {
	    $response = $client->request(
	      'GET', 
	      (($url)), 
	      [ 
	        'headers' => [
	          'Content-Type' => 'text/xml; charset=UTF8',
	          'Accept' => 'application/xml'
	        ], 
	        'timeout' => 50
	      ])->getBody()->getContents(); 
	  } catch (Exception $e) {
	    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
	  }
	  

	  if(isset($response)){
	    $ResponseCode = get_string_between(htmlspecialchars_decode($response), '<ResponseCode>', '</ResponseCode>');
	  } else {
	    $ResponseCode = '5';
	    $response = 'no';
	  }
	  /*
	  echo "Petición";
	  dump($url);
	  echo "Respuesta TN";
	  dump(htmlspecialchars_decode($response));
	  */
	  /*dump($Instr1); 
	  dump($Instr2); 
	  dump($ResponseCode); 
	  dump($DescripcionCode); 
	  dump($DateTime); 
	  dump($TransNumber); 
	  dump($AutoNo);*/

	  $peticion = new Peticion();
		  $peticion->peticion = $url;
		  $peticion->respuesta = htmlspecialchars_decode($response);
		  $peticion->responsecode = $ResponseCode;
		  $peticion->datetime = $DateTime;
		  $peticion->telefono = $tel;
		  $peticion->save();

	  if ($ResponseCode == 0) {
	    $role = "success";
	  } elseif ($ResponseCode == 6) {
	    sleep(5);
	    $string = "<QueryRequest><ID_GRP>".$userDist->id_grp."</ID_GRP><ID_CHAIN>".$userDist->id_chain."</ID_CHAIN><ID_MERCHANT>".$userDist->id_merchant."</ID_MERCHANT><ID_POS>".$userDist->id_pos."</ID_POS><DateTime>".$diahora."</DateTime><SKU>".$sku."</SKU><PhoneNumber>".$tel."</PhoneNumber><TransNumber>".$TransNumber."</TransNumber><ID_COUNTRY>0</ID_COUNTRY><TC>0</TC></QueryRequest>";
	    $url = $userDist->url.'ServicePX.asmx/getQueryClass?sXML='.($string);
	    $client = new \GuzzleHttp\Client(); 
	    $response = $client->request(
	      'GET', 
	      (($url)), 
	      [ 
	        'headers' => [
	          'Content-Type' => 'text/xml; charset=UTF8',
	          'Accept' => 'application/xml'
	        ], 
	        'timeout' => 50
	      ])->getBody()->getContents(); 
	    /*
	    echo "Petición Consulta";
	    dump($url);
	    echo "Respuesta Consulta TN";
	    dump(htmlspecialchars_decode($response));
	    */
	    $ResponseCode = get_string_between(htmlspecialchars_decode($response), '<ResponseCode>', '</ResponseCode>');

	    $peticion = new Peticion();
		  $peticion->peticion = $url;
		  $peticion->respuesta = htmlspecialchars_decode($response);
		  $peticion->responsecode = $ResponseCode;
		  $peticion->datetime = $DateTime;
		  $peticion->telefono = $tel;
		  $peticion->save();

	    if ($ResponseCode == 0) {
	      $role = "success";
	    } else {
	      $role = "danger";
	    }
	  } elseif ($ResponseCode == 71) {
	    sleep(5);
	    $string = "<QueryRequest><ID_GRP>".$userDist->id_grp."</ID_GRP><ID_CHAIN>".$userDist->id_chain."</ID_CHAIN><ID_MERCHANT>".$userDist->id_merchant."</ID_MERCHANT><ID_POS>".$userDist->id_pos."</ID_POS><DateTime>".$diahora."</DateTime><SKU>".$sku."</SKU><PhoneNumber>".$tel."</PhoneNumber><TransNumber>".$TransNumber."</TransNumber><ID_COUNTRY>0</ID_COUNTRY><TC>0</TC></QueryRequest>";
	    $url = $userDist->url.'ServicePX.asmx/getQueryClass?sXML='.($string);
	    $client = new \GuzzleHttp\Client(); 
	    $response = $client->request(
	      'GET', 
	      (($url)), 
	      [ 
	        'headers' => [
	          'Content-Type' => 'text/xml; charset=UTF8',
	          'Accept' => 'application/xml'
	        ], 
	        'timeout' => 50
	      ])->getBody()->getContents(); 
	    /*
	    echo "Petición Consulta";
	    dump($url);
	    echo "Respuesta Consulta TN";
	    dump(htmlspecialchars_decode($response));
	    */
	    $ResponseCode = get_string_between(htmlspecialchars_decode($response), '<ResponseCode>', '</ResponseCode>');
	    $peticion = new Peticion();
		  $peticion->peticion = $url;
		  $peticion->respuesta = htmlspecialchars_decode($response);
		  $peticion->responsecode = $ResponseCode;
		  $peticion->datetime = $DateTime;
		  $peticion->telefono = $tel;
		  $peticion->save();
	    if ($ResponseCode == 0) {
	      $role = "success";
	    } else {
	      $role = "danger";
	    }
	  } elseif($ResponseCode == 35 || $ResponseCode == 65 || $ResponseCode == 66 || $ResponseCode == 67 || $ResponseCode == 70 || $ResponseCode == 72 || $ResponseCode == 87 || $ResponseCode == 88 || $ResponseCode == 91 || $ResponseCode == 92 || $ResponseCode == 93 || $ResponseCode == 96 || $ResponseCode == 98 || $ResponseCode == 99 || $ResponseCode == 4 ){
	    $role = "danger";
	  } else {
	    sleep(5);
	    $string = "<QueryRequest><ID_GRP>".$userDist->id_grp."</ID_GRP><ID_CHAIN>".$userDist->id_chain."</ID_CHAIN><ID_MERCHANT>".$userDist->id_merchant."</ID_MERCHANT><ID_POS>".$userDist->id_pos."</ID_POS><DateTime>".$diahora."</DateTime><SKU>".$sku."</SKU><PhoneNumber>".$tel."</PhoneNumber><TransNumber>".$TransNumber."</TransNumber><ID_COUNTRY>0</ID_COUNTRY><TC>0</TC></QueryRequest>";
	    $url = $userDist->url.'ServicePX.asmx/getQueryClass?sXML='.($string);
	    $client = new \GuzzleHttp\Client(); 
	    $response = $client->request(
	      'GET', 
	      (($url)), 
	      [ 
	        'headers' => [
	          'Content-Type' => 'text/xml; charset=UTF8',
	          'Accept' => 'application/xml'
	        ], 
	        'timeout' => 50
	      ])->getBody()->getContents(); 
	    /*
	    echo "Petición Consulta";
	    dump($url);
	    echo "Respuesta Consulta TN";
	    dump(htmlspecialchars_decode($response));
	    */
	    $ResponseCode = get_string_between(htmlspecialchars_decode($response), '<ResponseCode>', '</ResponseCode>');
	    $peticion = new Peticion();
		  $peticion->peticion = $url;
		  $peticion->respuesta = htmlspecialchars_decode($response);
		  $peticion->responsecode = $ResponseCode;
		  $peticion->datetime = $DateTime;
		  $peticion->telefono = $tel;
		  $peticion->save();
	    if ($ResponseCode == 0) {
	      $role = "success";
	    } else {
	      $role = "danger";
	    }
	  }

	  if ($response != 'no') {
			$Instr1          = get_string_between(htmlspecialchars_decode($response), '<Instr1>', '</Instr1>');
			$Instr2          = get_string_between(htmlspecialchars_decode($response), '<Instr2>', '</Instr2>');
			$DescripcionCode = get_string_between(htmlspecialchars_decode($response), '<DescripcionCode>', '</DescripcionCode>');
			$DateTime        = get_string_between(htmlspecialchars_decode($response), '<DateTime>', '</DateTime>');
			$TransNumber     = get_string_between(htmlspecialchars_decode($response), '<TransNumber>', '</TransNumber>');
			$AutoNo          = get_string_between(htmlspecialchars_decode($response), '<AutoNo>', '</AutoNo>');
	  }

	  /*
	  echo "Instr1";
	  dump($Instr1); 
	  echo "Instr2";
	  dump($Instr2); 
	  echo "ResponseCode";
	  dump($ResponseCode); 
	  echo "DescriptionCode";
	  dump($DescripcionCode); 
	  echo "DateTime";
	  dump($DateTime); 
	  echo "TransNumber";
	  dump($TransNumber); 
	  echo "AutoNo";
	  dump($AutoNo);
	  */

	  $recarga = new Recarga();

	  Input::merge([
	  	'distribuidor_id' => $userDist->id, 
	  	'fecha' => date('Y-m-d H:i:s'), 
	  	'transnumber' => $TransNumber, 
	  	'phonenumber' => Input::get('telefono'), 
	  	'qty' => $monto->monto, 
	  	'autono' => $AutoNo, 
	  	'responsecode' => $ResponseCode, 
	  	'descriptioncode' => $DescripcionCode,
	  	'role' => $role,
	  	'instr1' => $Instr1,
	  	'instr2' => $Instr2,
	  ]);

	  //return dump(Input::all());

	  $id = $recarga->create(Input::all());

	  return Redirect::route('conecta.show',['id'=>$TransNumber]);




	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->layout = View::make('sistema.conecta.show');
		$this->layout->recarga = Recarga::where('transnumber',$id)->orderBy('created_at', 'desc')->first();
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
