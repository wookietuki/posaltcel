<?php

class TicketController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$distribuidorId        = Distribuidor::where('user_id',Sentry::getUser()->id)->first();
		$this->layout          = View::make('sistema.ticket.index');
		$this->layout->title   = 'Compra de Saldo';
		$this->layout->tickets = Ticket::where('distribuidor_id',$distribuidorId->id)->orderBy('fecha','desc')->limit(10)->get();
		$this->layout->distribuidor = $distribuidorId->id;
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Compra de Saldo',
		    'link'  => 'ticket',
		    'icon'  => 'fas fa-file-invoice-dollar'
		  ),
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout          = View::make('sistema.ticket.create');
		$this->layout->title   = 'Compra de Saldo';
		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Compra de Saldo',
		    'link'  => 'ticket',
		    'icon'  => 'fas fa-file-invoice-dollar'
		  ),
		);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$new = Input::all();
		$ticket = new Ticket();

		$distribuidor_id = Distribuidor::where('user_id', Sentry::getUser()->id)->first();
		//return dump($distribuidor_id->id);

		Input::merge(['distribuidor_id'=>$distribuidor_id->id]);
		//return dump(Input::all());

		if (!$ticket->validate($new)) {
      $errors = $ticket->errors();
      //return Redirect::route('ticket.index')->withInput()->withErrors($errors);
      return Redirect::back()->withInput()->withErrors($errors);
    }
		$date = date('Y-m-d');
    $destinationPath = 'upl/' .$date.'/';

    if (Input::file('foto')!=null) {
      $file      = Input::file('foto');
      $filename  = Shuffle::generateRandomString().$file->getClientOriginalName();
      $file->move($destinationPath, $filename);
      $savedPath = $destinationPath . $filename;
      Input::merge(['foto_ticket'=>$savedPath]);
    }

    if ($ticket->create(Input::all())) {
    	$data = array(
				'nombre'   => $distribuidor_id->user->first_name,
				'apellido' => $distribuidor_id->user->last_name,
				'cantidad' => Input::get('cantidad'),
				'folio'		 => Input::get('folio')
			);
			$mail = Mail::send('emails.registrado', $data, function($message)
			{
		    $message->to('stephanni_hernandez@altcel.com', 'Stephanni Hernández')->subject('Comprobante de pago registrado');
		    $message->to('mirza_chacon@altcel.com', 'Mirza Chacón')->subject('Comprobante de pago registrado');
		    $message->bcc('alejandro_rueda@altcel.com', 'Alejandro Rueda')->subject('Comprobante de pago registrado');
			});
    	return Redirect::route('ticket.index')->with('success','Se ha guardado el Comprobante de Pago.');
    }
    return Redirect::route('ticket.index')->with('error','Ha ocurrido un error! Intente de nuevo más tarde.')->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$ticket             = Ticket::find($id);
		$distribuidor       = Distribuidor::find(Input::get('dist'));
		$nombre             = $distribuidor->user->first_name.' '.$distribuidor->user->last_name;
		$email              = $distribuidor->user->email;
		$ticket->verificado = 1;
		if ($ticket->save()) {
			$data = array(
				'nombre'   => $distribuidor->user->first_name,
				'apellido' => $distribuidor->user->last_name,
				'cantidad' => $ticket->cantidad,
				'folio'		 => $ticket->folio
			);
			$mail = Mail::send('emails.verificado', $data, function($message) use ($nombre, $email)
			{
		    $message->to($email, $nombre)->subject('Comprobante de pago verificado');
		    $message->to('comprobantes_verificados@altcel.com', 'Comprobantes Verificados')->subject('Comprobante de pago verificado');
		    //$message->to('alejandro_rueda@altcel.com', 'Alejandro Rueda')->subject('Comprobante de pago verificado');
		    //$message->to('joel_maza@altcel.com', 'Joel Maza')->subject('Comprobante de pago verificado');
			});
			return Redirect::route('distribuidor.show', Input::get('dist'))->with('success','Se ha verificado el comprobante de pago.');
		}
	}

	public function updateAltcel2(){
		$id = Input::get('ticket');
		$amount = Input::get('amount');
		$dealer = Input::get('dealer');

		$ticket             = Ticket::find($id);
		$distribuidor       = Distribuidor::find($dealer);

		$nombre             = $distribuidor->user->first_name.' '.$distribuidor->user->last_name;
		$email              = $distribuidor->user->email;
		$ticket->verificado = 1;

		$saldo_conecta = $distribuidor->saldo_conecta;
		$saldo_conecta+=$amount;
		Distribuidor::where('id',$dealer)->update(['saldo_conecta'=>$saldo_conecta]);

		if ($ticket->save()) {
			$data = array(
				'nombre'   => $distribuidor->user->first_name,
				'apellido' => $distribuidor->user->last_name,
				'cantidad' => $ticket->cantidad,
				'folio'		 => $ticket->folio
			);
			$mail = Mail::send('emails.verificado', $data, function($message) use ($nombre, $email)
			{
		    $message->to($email, $nombre)->subject('Comprobante de pago verificado');
		    $message->to('comprobantes_verificados@altcel.com', 'Comprobantes Verificados')->subject('Comprobante de pago CONECTA verificado');
		    //$message->to('alejandro_rueda@altcel.com', 'Alejandro Rueda')->subject('Comprobante de pago verificado');
		    //$message->to('joel_maza@altcel.com', 'Joel Maza')->subject('Comprobante de pago verificado');
			});
			$response['http_code'] = 1;
			$response['message'] = 'Se ha verificado el comprobante de pago.';
			return $response;
		}
		// return $request;
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
