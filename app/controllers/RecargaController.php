<?php

class RecargaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$user = Sentry::getUser();
		$groupDist = Sentry::findGroupByName('Distribuidor');
		$userDist = Distribuidor::where('user_id',Sentry::getUser()->id)->first();

		//return dump($userDist);

		$this->layout        = View::make('sistema.recargas.index');
		$this->layout->title = 'Recargas';

		if ($user->inGroup($groupDist)){
			$this->layout->recargas = Recarga::where('distribuidor_id',$userDist->id)->whereDate('created_at', '>', Carbon::now()->subDays(30))->orderBy('fecha','desc')->get();
		} else {
			$this->layout->recargas = Recarga::whereDate('created_at', '>', Carbon::now()->subDays(30))->orderBy('fecha','desc')->get();
		}


		
		// add breadcrumb to current page
		$this->layout->breadcrumb = array(
		  array(
		    'title' => 'Inicio',
		    'link'  => '/',
		    'icon'  => 'fas fa-home'
		  ),
		  array(
		    'title' => 'Recargas',
		    'link'  => 'recarga',
		    'icon'  => 'fas fa-cash-register'
		  ),
		);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = Sentry::getUser();
		$groupDist = Sentry::findGroupByName('Distribuidor');
		$userDist = Distribuidor::where('user_id',Sentry::getUser()->id)->first();

		$validator = Validator::make(
		    array(
		        'telefono' => Input::get('telefono'),
		        'tel_conf' => Input::get('tel_conf'),
		        'monto' => Input::get('monto')
		    ),
		    array(
		        'telefono' => 'required|min:10|max:10',
		        'tel_conf' => 'required|min:10|max:10',
		        'monto' => 'required|numeric'
		    )
		);

		if ($validator->fails())
		{
		  return Redirect::back()->withInput()->withErrors($validator);
		}
		


		$urlsaldo = $userDist->url.'ServicePX.asmx/SaldoDisponible?lGrupo='.$userDist->id_grp.'&lCadena='.$userDist->id_chain.'&lTienda='.$userDist->id_merchant;


    $client = new \GuzzleHttp\Client(); 
    try {
      $response = $client->request(
        'GET', 
        (($urlsaldo)), 
        [ 
          'headers' => [
            'Content-Type' => 'text/xml; charset=UTF8',
            'Accept' => 'application/xml'
          ], 
          'timeout' => 50
        ])->getBody()->getContents(); 
    } catch (Exception $e) {
      echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    }


    function get_string_between($string, $start, $end){
      $string = ' ' . $string;
      $ini = strpos($string, $start);
      if ($ini == 0) return '';
      $ini += strlen($start);
      $len = strpos($string, $end, $ini) - $ini;
      return substr($string, $ini, $len);
    }
    if(isset($response)){
      $saldo = get_string_between(htmlspecialchars_decode($response), '<double xmlns="http://www.pagoexpress.com.mx/ServicePX">', '</double>');
    } else {
      $saldo = '0';
    }

    //$saldo = 10000;
    $monto = CantRecarga::find(Input::get('monto'));
		//return dump($monto);
    if ($saldo <= 0 || $monto->monto > $saldo) {
    	return Redirect::back()->with('error','El Saldo no es suficiente')->withInput();
    }


    //dump($saldo);
    //dump(Input::all());
    //dump($monto->monto);

    $monto = CantRecarga::find(Input::get('monto'));
    $role = "danger";
		$tel = Input::get('telefono');
		$sku = $monto->sku;
		$diahora = date('d/m/Y H:i:s');
		$TransNumber = rand ( 1 , 99999 );
		$DescripcionCode = "";
		$DateTime = $diahora;
		$AutoNo = 0;
		$Instr1 = "Su tarjeta no ha sido afectada con ningun cargo.";
		$Instr2 = "";

		$string = "<ReloadRequest><ID_GRP>".$userDist->id_grp."</ID_GRP><ID_CHAIN>".$userDist->id_chain."</ID_CHAIN><ID_MERCHANT>".$userDist->id_merchant."</ID_MERCHANT><ID_POS>".$userDist->id_pos."</ID_POS><DateTime>".$diahora."</DateTime><SKU>".$sku."</SKU><PhoneNumber>".$tel."</PhoneNumber><TransNumber>".$TransNumber."</TransNumber><ID_COUNTRY>0</ID_COUNTRY><TC>0</TC></ReloadRequest>";
  	
  	$url = $userDist->url.'ServicePX.asmx/getReloadClass?sXML='.($string);

  	$client = new \GuzzleHttp\Client(); 
	  try {
	    $response = $client->request(
	      'GET', 
	      (($url)), 
	      [ 
	        'headers' => [
	          'Content-Type' => 'text/xml; charset=UTF8',
	          'Accept' => 'application/xml'
	        ], 
	        'timeout' => 50
	      ])->getBody()->getContents(); 
	  } catch (Exception $e) {
	    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
	  }
	  

	  if(isset($response)){
	    $ResponseCode = get_string_between(htmlspecialchars_decode($response), '<ResponseCode>', '</ResponseCode>');
	  } else {
	    $ResponseCode = '5';
	    $response = 'no';
	  }
	  /*
	  echo "Petición";
	  dump($url);
	  echo "Respuesta TN";
	  dump(htmlspecialchars_decode($response));
	  */
	  /*dump($Instr1); 
	  dump($Instr2); 
	  dump($ResponseCode); 
	  dump($DescripcionCode); 
	  dump($DateTime); 
	  dump($TransNumber); 
	  dump($AutoNo);*/

	  $peticion = new Peticion();
		  $peticion->peticion = $url;
		  $peticion->respuesta = htmlspecialchars_decode($response);
		  $peticion->responsecode = $ResponseCode;
		  $peticion->datetime = $DateTime;
		  $peticion->telefono = $tel;
		  $peticion->save();

	  if ($ResponseCode == 0) {
	    $role = "success";
	  } elseif ($ResponseCode == 6) {
	    sleep(5);
	    $string = "<QueryRequest><ID_GRP>".$userDist->id_grp."</ID_GRP><ID_CHAIN>".$userDist->id_chain."</ID_CHAIN><ID_MERCHANT>".$userDist->id_merchant."</ID_MERCHANT><ID_POS>".$userDist->id_pos."</ID_POS><DateTime>".$diahora."</DateTime><SKU>".$sku."</SKU><PhoneNumber>".$tel."</PhoneNumber><TransNumber>".$TransNumber."</TransNumber><ID_COUNTRY>0</ID_COUNTRY><TC>0</TC></QueryRequest>";
	    $url = $userDist->url.'ServicePX.asmx/getQueryClass?sXML='.($string);
	    $client = new \GuzzleHttp\Client(); 
	    $response = $client->request(
	      'GET', 
	      (($url)), 
	      [ 
	        'headers' => [
	          'Content-Type' => 'text/xml; charset=UTF8',
	          'Accept' => 'application/xml'
	        ], 
	        'timeout' => 50
	      ])->getBody()->getContents(); 
	    /*
	    echo "Petición Consulta";
	    dump($url);
	    echo "Respuesta Consulta TN";
	    dump(htmlspecialchars_decode($response));
	    */
	    $ResponseCode = get_string_between(htmlspecialchars_decode($response), '<ResponseCode>', '</ResponseCode>');

	    $peticion = new Peticion();
		  $peticion->peticion = $url;
		  $peticion->respuesta = htmlspecialchars_decode($response);
		  $peticion->responsecode = $ResponseCode;
		  $peticion->datetime = $DateTime;
		  $peticion->telefono = $tel;
		  $peticion->save();

	    if ($ResponseCode == 0) {
	      $role = "success";
	    } else {
	      $role = "danger";
	    }
	  } elseif ($ResponseCode == 71) {
	    sleep(5);
	    $string = "<QueryRequest><ID_GRP>".$userDist->id_grp."</ID_GRP><ID_CHAIN>".$userDist->id_chain."</ID_CHAIN><ID_MERCHANT>".$userDist->id_merchant."</ID_MERCHANT><ID_POS>".$userDist->id_pos."</ID_POS><DateTime>".$diahora."</DateTime><SKU>".$sku."</SKU><PhoneNumber>".$tel."</PhoneNumber><TransNumber>".$TransNumber."</TransNumber><ID_COUNTRY>0</ID_COUNTRY><TC>0</TC></QueryRequest>";
	    $url = $userDist->url.'ServicePX.asmx/getQueryClass?sXML='.($string);
	    $client = new \GuzzleHttp\Client(); 
	    $response = $client->request(
	      'GET', 
	      (($url)), 
	      [ 
	        'headers' => [
	          'Content-Type' => 'text/xml; charset=UTF8',
	          'Accept' => 'application/xml'
	        ], 
	        'timeout' => 50
	      ])->getBody()->getContents(); 
	    /*
	    echo "Petición Consulta";
	    dump($url);
	    echo "Respuesta Consulta TN";
	    dump(htmlspecialchars_decode($response));
	    */
	    $ResponseCode = get_string_between(htmlspecialchars_decode($response), '<ResponseCode>', '</ResponseCode>');
	    $peticion = new Peticion();
		  $peticion->peticion = $url;
		  $peticion->respuesta = htmlspecialchars_decode($response);
		  $peticion->responsecode = $ResponseCode;
		  $peticion->datetime = $DateTime;
		  $peticion->telefono = $tel;
		  $peticion->save();
	    if ($ResponseCode == 0) {
	      $role = "success";
	    } else {
	      $role = "danger";
	    }
	  } elseif($ResponseCode == 35 || $ResponseCode == 65 || $ResponseCode == 66 || $ResponseCode == 67 || $ResponseCode == 70 || $ResponseCode == 72 || $ResponseCode == 87 || $ResponseCode == 88 || $ResponseCode == 91 || $ResponseCode == 92 || $ResponseCode == 93 || $ResponseCode == 96 || $ResponseCode == 98 || $ResponseCode == 99 || $ResponseCode == 4 ){
	    $role = "danger";
	  } else {
	    sleep(5);
	    $string = "<QueryRequest><ID_GRP>".$userDist->id_grp."</ID_GRP><ID_CHAIN>".$userDist->id_chain."</ID_CHAIN><ID_MERCHANT>".$userDist->id_merchant."</ID_MERCHANT><ID_POS>".$userDist->id_pos."</ID_POS><DateTime>".$diahora."</DateTime><SKU>".$sku."</SKU><PhoneNumber>".$tel."</PhoneNumber><TransNumber>".$TransNumber."</TransNumber><ID_COUNTRY>0</ID_COUNTRY><TC>0</TC></QueryRequest>";
	    $url = $userDist->url.'ServicePX.asmx/getQueryClass?sXML='.($string);
	    $client = new \GuzzleHttp\Client(); 
	    $response = $client->request(
	      'GET', 
	      (($url)), 
	      [ 
	        'headers' => [
	          'Content-Type' => 'text/xml; charset=UTF8',
	          'Accept' => 'application/xml'
	        ], 
	        'timeout' => 50
	      ])->getBody()->getContents(); 
	    /*
	    echo "Petición Consulta";
	    dump($url);
	    echo "Respuesta Consulta TN";
	    dump(htmlspecialchars_decode($response));
	    */
	    $ResponseCode = get_string_between(htmlspecialchars_decode($response), '<ResponseCode>', '</ResponseCode>');
	    $peticion = new Peticion();
		  $peticion->peticion = $url;
		  $peticion->respuesta = htmlspecialchars_decode($response);
		  $peticion->responsecode = $ResponseCode;
		  $peticion->datetime = $DateTime;
		  $peticion->telefono = $tel;
		  $peticion->save();
	    if ($ResponseCode == 0) {
	      $role = "success";
	    } else {
	      $role = "danger";
	    }
	  }

	  if ($response != 'no') {
			$Instr1          = get_string_between(htmlspecialchars_decode($response), '<Instr1>', '</Instr1>');
			$Instr2          = get_string_between(htmlspecialchars_decode($response), '<Instr2>', '</Instr2>');
			$DescripcionCode = get_string_between(htmlspecialchars_decode($response), '<DescripcionCode>', '</DescripcionCode>');
			$DateTime        = get_string_between(htmlspecialchars_decode($response), '<DateTime>', '</DateTime>');
			$TransNumber     = get_string_between(htmlspecialchars_decode($response), '<TransNumber>', '</TransNumber>');
			$AutoNo          = get_string_between(htmlspecialchars_decode($response), '<AutoNo>', '</AutoNo>');
	  }

	  /*
	  echo "Instr1";
	  dump($Instr1); 
	  echo "Instr2";
	  dump($Instr2); 
	  echo "ResponseCode";
	  dump($ResponseCode); 
	  echo "DescriptionCode";
	  dump($DescripcionCode); 
	  echo "DateTime";
	  dump($DateTime); 
	  echo "TransNumber";
	  dump($TransNumber); 
	  echo "AutoNo";
	  dump($AutoNo);
	  */

	  $recarga = new Recarga();

	  Input::merge([
	  	'distribuidor_id' => $userDist->id, 
	  	'fecha' => date('Y-m-d H:i:s'), 
	  	'transnumber' => $TransNumber, 
	  	'phonenumber' => Input::get('telefono'), 
	  	'qty' => $monto->monto, 
	  	'autono' => $AutoNo, 
	  	'responsecode' => $ResponseCode, 
	  	'descriptioncode' => $DescripcionCode,
	  	'role' => $role,
	  	'instr1' => $Instr1,
	  	'instr2' => $Instr2,
	  ]);

	  //return dump(Input::all());

	  $id = $recarga->create(Input::all());

	  return Redirect::route('recarga.show',['id'=>$TransNumber]);

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->layout = View::make('sistema.recargas.show');
		$this->layout->recarga = Recarga::where('transnumber',$id)->orderBy('created_at', 'desc')->first();
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
