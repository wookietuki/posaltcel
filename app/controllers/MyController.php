<?php

class MyController extends \BaseController {

	public function getIndex()
	{
		//return json_encode(['info' => 'El distribuidor se ha guardado con exito <br> Usuario: USUARIO <br>Contraseña: secreto <br>Por favor pida al Distribuidor que cambie su contraseña inmediatamente.']);

		$new = [
			'email'      => $_POST['email'],
			'password'   => 'secreto',
			'activated'  => true,
			'username'   => $_POST['username'],
			'first_name' => $_POST['first_name'],
			'last_name'  => $_POST['last_name'],
		    "distribuidor" => $_POST['distribuidor'],
		    "telefono" => $_POST['telefono'],
		    "id_grp" => $_POST['id_grp'],
		    "id_chain" => $_POST['id_chain'],
		    "id_merchant" => $_POST['id_merchant'],
		    "id_pos" => $_POST['id_pos'],
		    "url" => $_POST['url'],
		    "observaciones" => $_POST['observaciones']
		];
		$distribuidor = new Distribuidor();
		//return 'hola';
		//return $new;

		if (!$distribuidor->validate($new))
		{
	    $errors = $distribuidor->errors();
	    return $errors;
		}

		//return 'hola';
		//return json_encode(['error','algun error']);
		try
		{
	    // Create the user
	    $user = Sentry::createUser(array(
				'email'      => Input::get('email'),
				'password'   => 'secreto',
				'activated'  => true,
				'username'   => Input::get('username'),
				'first_name' => Input::get('first_name'),
				'last_name'  => Input::get('last_name'),
	    ));

	    //return json_decode(['error','algun error']);
	    // Find the group using the group id
	    $distribuidorGroup = Sentry::findGroupByName('Distribuidor');

	    // Assign the group to the user
	    $user->addGroup($distribuidorGroup);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		  //echo 'Login field is required.';
		  return json_encode(['error'=>'Login']);
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		  //echo 'Password field is required.';
		  return json_encode(['error'=>'Password']);
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		  //echo 'User with this login already exists.';
		  return json_encode(['error'=>'Ya existe el usuario']);
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		  //echo 'Group was not found.';
		  return json_encode(['error'=>'Grupo']);
		}
		catch (Exception $e)
		{
			if($e->errorInfo[0] === '23000'){
				return json_encode(['error'=>'Ya esta siendo usado el correo']);
			} else{
				return json_encode(['error'=>'Ha ocurrido un error, intente de nuevo más tarde']);
			}
		}

		Input::merge(['user_id'=>$user->id]);
		Distribuidor::create(Input::all());
		return json_encode(['info'=>'El distribuidor se ha guardado con exito <br> Usuario: '.$user->username.' <br>Contraseña: secreto <br>Por favor pida al Distribuidor que cambie su contraseña inmediatamente.']);
	}

}